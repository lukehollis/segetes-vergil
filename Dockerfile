# build environment
FROM node:0.10.31

# set working directory
RUN mkdir /app
COPY . /app/.
WORKDIR /app

# install and cache app dependencies
RUN npm i


# prepare runtime
CMD ["node", "server.js"]
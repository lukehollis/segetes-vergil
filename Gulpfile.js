// include gulp
var 	gulp      = require('gulp') 
 
// include plug-ins
,	  jshint      = require('gulp-jshint')
,	  changed     = require('gulp-changed')
,	  imagemin    = require('gulp-imagemin')
,	  concat      = require('gulp-concat')
,	  stripDebug  = require('gulp-strip-debug')
,	  compass     = require('gulp-compass')
,	  uglify      = require('gulp-uglify')
,	  autoprefix  = require('gulp-autoprefixer')
,	  minifyCSS   = require('gulp-minify-css')
,   browserify  = require('gulp-browserify')
,   nodemon     = require('gulp-nodemon')
,   imageResize = require('gulp-image-resize')
,   notify      = require('gulp-notify')
,   rename      = require('gulp-rename')
,   plumber     = require('gulp-plumber')
,   livereload  = require('gulp-livereload')
;
 
// JS hint task
gulp.task('jshint', function() {
	gulp.src(['./themes/**/client/*.js', './themes/**/client/**/*.js', './packages/**/*.js'])
    .pipe(plumber())
		.pipe(jshint())
		.pipe(jshint.reporter('default'));
});

// Minify new images
gulp.task('imagemin', function() {
  var imgSrc = ['./img/*', './img/*/*']
  ,   imgDst = './static/img'
  ;
 
  gulp.src(imgSrc)
    .pipe(plumber())
    .pipe(changed(imgDst))
    .pipe(imagemin())
    .pipe(gulp.dest(imgDst));
});

// Make small thumbnails
gulp.task('thumbnails_small', function() {

  var imgSrc = ['./img/*', './img/*/*']
  ,   imgDst = './static/img'
  ;

  gulp.src(imgSrc)
    .pipe(plumber())
    .pipe(changed(imgDst))
    .pipe(imageResize({ 
      width : 400,
      height : 400,
      crop : true,
      upscale : true 
    }))
    .pipe(rename(function (path) {
        path.basename += "-small";
    }))
    .pipe(imagemin())
    .pipe(gulp.dest(imgDst));

});

// Make large thumbnails
gulp.task('thumbnails_large', function() {

  var imgSrc = ['./img/*', './img/*/*']
  ,   imgDst = './static/img'
  ;

  gulp.src(imgSrc)
    .pipe(plumber())
    .pipe(changed(imgDst))
    .pipe(imageResize({ 
      width : 1000,
      height : 700,
      crop : true,
      upscale : false
    }))
    .pipe(rename(function (path) {
        path.basename += "-large";
    }))
    .pipe(imagemin())
    .pipe(gulp.dest(imgDst));

});

// Make uncropped large thumbnails
gulp.task('thumbnails_large_uncropped', function() {

  var imgSrc = ['./img/*', './img/*/*']
  ,   imgDst = './static/img'
  ;

  gulp.src(imgSrc)
    .pipe(plumber())
    .pipe(changed(imgDst))
    .pipe(imageResize({ 
      width : 1000,
      crop : false,
      upscale : false
    }))
    .pipe(rename(function (path) {
        path.basename += "-large-uncropped";
    }))
    .pipe(imagemin())
    .pipe(gulp.dest(imgDst));

});

// JS concat, strip debugging and minify
gulp.task('scripts', function() {
  gulp.src(['./themes/**/client/*.js', './themes/**/client/**/*.js'])

    // plumber for errors
    .pipe(plumber())

    // finally, concatenate files
    .pipe(concat('app.js'))
    // .pipe(stripDebug())
    // .pipe(uglify())
    .pipe(gulp.dest('./static/js'));
});

// Process compass scss to css
gulp.task('compass', function() {
  gulp.src('./scss/*.scss')
  .pipe(plumber())
  .pipe(compass({
    config_file: './config.rb',
    css: 'static/css_src',
    sass: 'scss'
  }))
  .pipe(gulp.dest('./static/css_src'));
});

// CSS concat, auto-prefix and minify
gulp.task('styles', function() {
  gulp.src(['./bower_components/html5-boilerplate/css/normalize.css', './bower_components/html5-boilerplate/css/main.css', './static/css_src/base.css'])
    .pipe(plumber())
    .pipe(concat('styles.css'))
    .pipe(autoprefix('last 2 versions'))
    .pipe(minifyCSS())
    .pipe(gulp.dest('./static/css'))
    .pipe(livereload());
});

// nodemon for development
gulp.task('nodemon', function() {

  // set livereload to listen
  livereload.listen();

  nodemon({ script: 'server.js', ext: 'jade js', ignore: ['./static/js/*.js'] })
    .on('restart', function (fname) {

      // trigger livereload
      setTimeout(livereload.changed, 2000);
      console.log(' -- nodemon:  server.js restart - ', fname);
    })
});

// default task
gulp.task('default', ['imagemin', 'scripts', 'compass', 'styles', 'nodemon'], function() {

  // watch for JS changes
  gulp.watch(['./themes/**/client/*.js', './themes/**/client/**/*.js'], ['scripts']);
 
  // watch for CSS changes
  gulp.watch('./themes/**/scss/*.scss', ['compass']);
  gulp.watch('./static/css_src/base.css', ['styles']);

});

// make thumbnails
gulp.task('thumbnails', ['thumbnails_small', 'thumbnails_large', 'thumbnails_large_uncropped'], function() {

  gulp.watch('./img/*', ['thumbnails_small', 'thumbnails_large', 'thumbnails_large_uncropped']);

});

// production 
// cleanup for deploying
gulp.task('production', ['imagemin', 'scripts', 'compass', 'styles'], function() {
  // watch for JS changes
  gulp.watch(['./themes/**/client/*.js', './themes/**/client/**/*.js'], ['scripts']);
 
  // watch for CSS changes
  gulp.watch('./scss/*.scss', ['compass']);
  gulp.watch('./static/css_src/base.css', ['styles']);

});

var controllers = {

	admin : function(req, res) {
		res.render("admin/index", {
			user : req.current_user,
			page_class : "admin", 
			page_title : "Admin"});
	},
	
	login : function(req, res) {
		res.render("admin/login", {
			user : req.current_user,
			page_class : "login", 
			page_title : "Login"});
	},

	upsert : function(req, res) {
		api(req, res)
			.upsert();
	},


};

module.exports = controllers;

/* 
 * Controllers.js
 *
 * Controllers for application, mitigating between routes, templates, and other server functionality 
 *
 */

var Api = require("./lib/api/api")
;

var controllers = {

	index : function(req, res) {
		res.render("index", {
			user : req.current_user,
			page_class : "index", 
			page_title : "Home"
        });
	},


	search : function(req, res) {
		res.render("search", {
			user : req.current_user,
			page_class : "search", 
			page_title : "Search"
        });
	},

	error_page : function(req, res) {
		res.render("404", {
			user : req.current_user,
			page_class : "404", 
			page_title : "Sure this is a page?"
        });
	},

	auth_check : function(req, res){
		if ( req.current_user !== null ){
			res.send(true);
		}else{
			res.send(false);
		}
	},

	query : function(req, res) {
		var api = new Api(req, res);
		api.query();
	},

};

module.exports = controllers;
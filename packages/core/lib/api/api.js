/* 
 * Api.js
 *
 * Restful api for exposing access to the database for the client app 
 *
 */

var Mongo = require("./mongo")
,	Postgres = require("./postgres")

,	Api = function(req, res){

		var segetes_config = global.segetes.config

		// Load the models from the active packages
		this.models = this.models || require("../util").load_models();

		// Create the correct api ORM class instance  
		if ( segetes_config.db.engine === "mongo" ){
			this.mongo = new Mongo( this.models, req, res );

		} else if ( segetes_config.db.engine === "postgres" ){
			this.postgres = new Postgres( this.models, req, res );

		}

		// Query/read
		this.query = function ( ) {

			// Handle the query with the correct ORM 
			if ( segetes_config.db.engine === "mongo" ){
				this.mongo.query( );

			} else if ( segetes_config.db.engine === "postgres" ){
				this.postgres.query( );
			}

		};

		// Upsert/create/update
		this.upsert = function( ) 	{

			// Handle the upsert with the correct ORM 
			if ( segetes_config.db.engine === "mongo" ){
				this.mongo.upsert(  );

			} else if ( segetes_config.db.engine === "postgres" ){
				this.postgres.upsert(  );
			}

		};

		// Delete
		this.delete = function( ) 	{

			// Handle the delete with the correct ORM 
			if ( segetes_config.db.engine === "mongo" ){
				this.mongo.delete(  );

			} else if ( segetes_config.db.engine === "postgres" ){
				this.postgres.delete( );
			}

		};

	}
;

module.exports = Api;


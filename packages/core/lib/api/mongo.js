var	async = require("async")
,	extend = require("util")._extend
;

// Manage API queries from mongo
var Mongo = function( models, req, res){

		var textsearch = null
		,	limit = 0
		,	offset = 0
		,	include_line_refs = false
		,	sort = {}
		,	that = this
		;

		// query mongo for the selected content type and params 
		this.query = function ( ) {

			if ( this.process_params() ){

				console.log( "Mongo: -- Query params:", params, "req.body:", req.body );

				if ( params.manifest ) {

					req.body.model = "manifest"; 
					query = models["lines"].distinct("author");	
					query.exec( this.get_manifest );


				} else { 

					query = models[ req.body.model ].find( params );

					// perform text search 
					if ( textsearch !== null ){
						query.or([{ "line.text" : { $regex: textsearch }}]);
					}

					// limit the query results
					query.skip( offset ).limit( limit );	
						
					// query updated 
					query.sort( sort );

					if ( include_line_refs ){
						query.exec( this._sub_line_query );

					}else {
						query.exec( this.send_response );

					}

				}

			}

			// reset params and filters for next query or upsert
			this.reset_params();
		};

		// Upsert data to mongo collection
		this.upsert = function( ) 	{

			if (this.process_params()){

				console.log("Mongo: -- upsert:", params );

				if (typeof params._id !== "undefined"){

					delete params._id;
					models[req.body.model]
						.update({_id:req.body._id}, params, {}, this.send_response);

				}else{

					models[req.body.model]
						.create(params, this.send_response);

				}

			}

			// Reset params and filters for next query or upsert
			this.reset_params();
		};

		this.delete = function ( ) {
			if (this.process_params()){
				console.log("Mongo: -- deleted:", params);
			}
		};

		// finally, set the response from the query or upsert
		this.send_response = function( err, query_results ) {

			// set correct headers
			res.header("Access-Control-Allow-Origin", "http://localhost");
			res.header("Access-Control-Allow-Methods", "GET, POST");
			res.header("Content-Type", "application/json");

			results = {
				'model' : req.body.model,
				'res' : query_results
			};

			if (typeof req.body.load_post !== "undefined") {
				results.load_post = req.body.load_post;
			}

			if (typeof req.body.__resp__ !== "undefined") {
				results.__resp__ = req.body.__resp__;
			}

			if (typeof req.body.limit !== "undefined") {
				results.limit = req.body.limit;
			}

			if (typeof req.body.__bind_method__ !== "undefined") {
				results.__bind_method__ = req.body.__bind_method__;
			}

			if (typeof req.body.include_line_refs !== "undefined") {
				results.include_line_refs = req.body.include_line_refs;
			}


			res.json(results);
		};

		// sanitize and divide the params for the query or upsert 
		this.process_params = function () {

			// If it"s a post the the api, retrieve query from req.body
			params = extend( {}, req.body );

			if ( typeof params.manifest === "undefined" || params.manifest == false ){
			// If it"s not a manifest query

				// model will be referenced via req.body
				delete params.model;
				if( typeof req.body.model === "undefined" || req.body.model === "users" ){
					req.body.model = "lines";
				}

				// limit should be moved to filter
				if (typeof req.body.limit !== "undefined") {
					limit = parseInt( req.body.limit );
					delete params.limit;

				}else {
					limit = 15;
				}

				if (typeof req.body.sort !== "undefined") {
					sort = req.body.sort;
					delete params.sort;

				}

				if (typeof req.body.offset !== "undefined") {
					offset = parseInt( req.body.offset );
					delete params.offset;

				}

				if (typeof req.body.include_line_refs !== "undefined") {
					include_line_refs = req.body.include_line_refs; 
					delete params.include_line_refs;

				}

				if (typeof req.body.__bind_method__ !== "undefined") {
					delete params.__bind_method__;

				}

				if (typeof req.body.textsearch !== "undefined") {
					textsearch = req.body.textsearch;
					delete params.textsearch;

				}

			}


			delete params.__resp__;

			sanitized = true;	
			return sanitized;

		};


		this._sub_line_query = function( err, results ){
			var ids = [0]
			;


			original_query = results;

			if ( results.length ){
				ids = results[0].lines;	
			}

			sub_query = models["lines"]
				.find({ _id : {
								$in : ids
							},
						},{
							_id : 1,
							work : 1,
							subwork : 1,
							line : 1
						});

			sub_query.sort({ "work" : 1, "subwork.n" : 1, "line.n" : 1});

			sub_query.exec( function( err, results ){

				if ( original_query.length > 0 ){
					original_query[0].lines = JSON.stringify( results );
					that.send_response( false, original_query );

				} else {
					that.send_response( false, [] );

				}

			}); 

		}

		// Get manifest works
		this.get_manifest  = function( err, results ){
			var work_queries = []
			;

			query = models["lines"].distinct("author");					

			// For each author
			results.forEach( function( author ){

				// Add queries for each work
				work_queries.push(
						function( callback ) { 

							// Query each work
							models.lines.distinct("work", { author : author }, function(err, results){

									var subwork_queries = [];

									// For each work, add queries for each subwork
									results.forEach( function( work ) {

										// Add queries for each subwork
										subwork_queries.push(
												function( cb ) { 

													// Query each subwork
													models.lines.distinct("subwork", { work : work, author : author }, function(err, results){

															var author_is_in_manifest = false;

															// Finally, add the data to the manifest
															manifest.forEach(function(a){

																	if ( a.author === author ){
																		author_is_in_manifest = true;

																		a.works.push({
																				work : work,
																				subworks : results 
																			});

																	}

																});

															// Create author in manifest if author not found
															if ( !author_is_in_manifest ){
																manifest.push({ author : author,
																				works : [{
																					work : work,
																					subworks : results 
																				}]
																			});

															}


														cb();

													});
												});

									});

									async.parallel( subwork_queries, function() {
										callback();	
									});

								});
						});

			});

			async.parallel( work_queries, function() {
				that.send_response( false, manifest );
			});

		};


		// helper function for resetting various params
		this.reset_params = function() {
			params = {};
			filters = {};
			sort = {};
			limit = 0;
			offset = 0;
			textsearch = null;
			include_line_refs = false;
			original_query = [];
		};

};
module.exports = Mongo;
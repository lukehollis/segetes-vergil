// Manage API queries from postgres 
var Postgres = function(req, res){

		// Load the models from the active packages
		var models = require("../util").load_models();

		// query mongo for the selected content type and params 
		this.query = function ( ) {

			console.log(models);

			if ( this.process_params() ){

				console.log("Querying params", params);

				if ( params.manifest ) {

					req.body.model = "manifest"; 
					query = models["lines"].distinct("author");					
					query.exec(this.get_manifest);


				} else { 

					query = models[req.body.model]
						.find(params);

					// perform text search 
					if (textsearch !== null){

						query.or([{ "line.text" : { $regex: textsearch }}]);
					}

					// limit the query results
					query.skip( offset ).limit( filters.limit );	
						
					// query updated 
					query.sort( sort );

					if ( with_lines ){
						query.exec( this.extra_line_query );

					}else {
						query.exec( this.send_response );

					}

				}

			}

			// reset params and filters for next query or upsert
			this.reset_params();
		};

		// upsert data to mongo collection
		this.upsert = function( ) 	{
			if (this.process_params()){

				if (typeof params._id !== "undefined"){
					delete params._id;
					models[req.body.model]
						.update({_id:req.body._id}, params, {}, this.send_response);

				}else{
					models[req.body.model]
						.create(params, this.send_response);

				}

			}

			// reset params and filters for next query or upsert
			this.reset_params();
		};

		this.delete = function ( ) {
			console.log("Mongo: -- deleted:", req.body);
		};

		// sanitize and divide the params for the query or upsert 
		this.process_params = function () {

			if (req.method === "POST") {
				// If it"s a post the the api, retrieve query from req.body
				params = extend( {}, req.body );

				if ( typeof params.manifest === "undefined" ){
				// If it"s not a manifest query

					// model will be referenced via req.body
					delete params.model;
					if( typeof req.body.model === "undefined" || req.body.model === "users" ){
						req.body.model = "lines";
					}

					// limit should be moved to filter
					if (typeof req.body.limit !== "undefined") {
						filters.limit = parseInt( req.body.limit );
						delete params.limit;

					}else {
						filters.limit = 100;

					}

					if (typeof req.body.sort !== "undefined") {
						sort = req.body.sort;
						delete params.sort;

					}

					if (typeof req.body.offset !== "undefined") {
						offset = parseInt( req.body.offset );
						delete params.offset;

					}

					if (typeof req.body.with_lines !== "undefined") {
						with_lines = req.body.with_lines; 
						delete params.with_lines;

					}

					if (typeof req.body.__bind_method__ !== "undefined") {
						delete params.__bind_method__;

					}

					if (typeof req.body.textsearch !== "undefined") {
						textsearch = req.body.textsearch;
						delete params.textsearch;

					}

				}

				sanitized = true;

			}else {
				// If it"s not a post to the api, just process the path variables
				path = req.params["0"].split("/");

				// If the params are a string, means it"s a blank api query
				if ( path.length === 1 ){
					params = {manifest:true};

				}else{
				// It"s a potential api query
					if ( path.length > 4 ){
						path_lines = path[3].split("-");

						if ( path_lines.length > 1 ){
							params["line.n"] = {
								$gte : path_lines[0],
								$lte : path_lines[1]
							}
						}else {
							params["line.n"] = {
								$gte : path_lines[0]
							}
						}

						params["subwork.n"] = parseInt( path[2] );
						params["work"] = path[1]; 
						req.body.model = path[0];	

					}else if ( path.length > 3 ){
						params["work"] = path[1]; 
						params["subwork.n"] = parseInt( path[2] );
						params["line.n"] = {
							$gte : 1
						}
						req.body.model = path[0];	

					}else if (path.length > 2) {
						params["work"] = path[1]; 
						params["subwork.n"] = 1; 
						params["line.n"] = {
							$gte : 1
						}
						req.body.model = path[0];	

					}else if (path.length > 1) {
						params["work"] = path[1]; 
						params["subwork.n"] = 1; 
						params["line.n"] = {
							$gte : 1
						}
						req.body.model = path[0];	

					}else if (path.length > 1) {
						req.body.model = path[0];	
						params["work"] = ""; 
						params["subwork.n"] = 1; 
						params["line.n"] = {
							$gte : 1
						}

					}else {
						req.body.model = "lines";	
						params["work"] = ""; 
						params["subwork.n"] = 1; 
						params["line.n"] = {
							$gte : 1
						}
					}

					// Handle a few redirects for other data types
					if ( path.length > 0 ){
						if ( path[0] === "tei" ){
							res.redirect(301, "https://github.com/segetes/segetes_data/tree/master/tei");
							return false;
						}else if ( path[0] === "jsonld" ){
							res.redirect(301, "https://github.com/segetes/segetes_data/tree/master/jsonld");
							return false;
						}
					}

					// limits
					filters.limit = 15; 

				}

				sanitized = true;
			}

			delete params.__resp__;
			
			return sanitized;

		};


		this.extra_line_query = function( err, results ){
			var ids = [0]
			;


			original_query = results;

			if ( results.length ){
				ids = results[0].lines;	
			}

			sub_query = models["lines"]
				.find({ _id : {
								$in : ids
							},
						},{
							_id : 1,
							work : 1,
							subwork : 1,
							line : 1
						});

			sub_query.sort({ "work" : 1, "subwork.n" : 1, "line.n" : 1});

			sub_query.exec( function( err, results ){
				if ( original_query.length > 0 ){
					original_query[0].lines = JSON.stringify( results );
					that.send_response( false, original_query );
				} else {
					that.send_response( false, [] );
				}
			}); 

		}

		// Get manifest works
		this.get_manifest  = function( err, results ){
			var work_queries = [];

			query = models["lines"].distinct("author");					

			// For each author
			results.forEach( function( author ){

				// Add queries for each work
				work_queries.push(
						function( callback ) { 

							// Query each work
							models.lines.distinct("work", { author : author }, function(err, results){

									var subwork_queries = [];

									// For each work, add queries for each subwork
									results.forEach( function( work ) {

										// Add queries for each subwork
										subwork_queries.push(
												function( cb ) { 

													// Query each subwork
													models.lines.distinct("subwork", { work : work, author : author }, function(err, results){

															var author_is_in_manifest = false;

															// Finally, add the data to the manifest
															manifest.forEach(function(a){

																	if ( a.author === author ){
																		author_is_in_manifest = true;

																		a.works.push({
																				work : work,
																				subworks : results 
																			});

																	}

																});

															// Create author in manifest if author not found
															if ( !author_is_in_manifest ){
																manifest.push({ author : author,
																				works : [{
																					work : work,
																					subworks : results 
																				}]
																			});

															}


														cb();

													});
												});

									});

									async.parallel( subwork_queries, function() {
										callback();	
									});

								});
						});

			});

			async.parallel( work_queries, function() {
				that.send_response( false, manifest );
			});

		};


		// helper function for resetting various params
		this.reset_params = function() {
			params = {};
			filters = {};
			sort = {};
			limit = 0;
			offset = 0;
			textsearch = null;
			with_lines = false;
			original_query = [];
		};

};
module.exports = Postgres;
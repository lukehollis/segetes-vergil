/* 
 * Authentication.js
 *
 * Login/signup for designed strategies 
 *
 */

var passport = require("passport")
,   LocalStrategy = require("passport-local").Strategy
,   FacebookStrategy = require("passport-facebook").Strategy
,   TwitterStrategy  = require("passport-twitter").Strategy
,   GithubStrategy  = require("passport-github").Strategy
,   GoogleStrategy = require("passport-google-oauth").OAuth2Strategy
,   User = require("../models").users
,   oauth_config = require("../oauth_config")
,   ObjectId = require("mongoose").Types.ObjectId
;



// Serialize the user for the session
passport.serializeUser(function(user, done) {
    done(null, user.id);
});

// Deserialize the user
passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
        done(err, user);
    });
});

/*
 *
 * Passport Local Signup/Login configurations
 *
 */ 
// Signup for Local
passport.use("local-signup", new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : "email",
        passwordField : "password",
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, email, password, done) {

        // asynchronous
        // User.findOne wont fire unless data is sent back
        process.nextTick(function() {

        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        User.findOne({ "local.email" :  email }, function(err, user) {
            // if there are any errors, return the error
            if (err)
                return done(err);

            // check to see if theres already a user with that email
            if (user) {
                return done(null, false, function(){req.session.messages = ["That email is already taken."]});
            } else {

                // if there is no user with that email
                // create the user
                var newUser            = new User();

                // set the user"s local credentials
                newUser.local.name     = req.body.name;
                newUser.local.email    = email;
                newUser.local.password = newUser.generateHash(password);

                // save the user
                newUser.save(function(err) {
                    if (err)
                        throw err;
                    return done(null, newUser);
                });
            }

        });    

        });

    }));

// Login for Local
passport.use("local-login", new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField : "email",
        passwordField : "password",
        passReqToCallback : true // allows us to pass back the entire request to the callback
    },
    function(req, email, password, done) { // callback with email and password from our form

        // find a user whose email is the same as the forms email
        // we are checking to see if the user trying to login already exists
        User.findOne({ "local.email" :  email }, function(err, user) {
            // if there are any errors, return the error before anything else
            if (err)
                return done(err);

            // if no user is found, return the message
            if (!user)
                return done(null, false, function(){req.session.messages = ["loginMessage", "No user found."]}); 

            // if the user is found but the password is wrong
            if (!user.validPassword(password))
                return done(null, false, function(){req.session.messages = ["loginMessage", "Oops! Wrong password."]}); 

            // all is well, return successful user
            return done(null, user);
        });

    }));

/*
 *
 * Passport Google Login configurations
 *
 */ 
 passport.use(new GoogleStrategy({

        clientID        : oauth_config.google.clientID,
        clientSecret    : oauth_config.google.clientSecret,
        callbackURL     : oauth_config.google.callbackURL,

    },
    function(token, refreshToken, profile, done) {


        // make the code asynchronous
        // User.findOne won"t fire until we have all our data back from Google
        process.nextTick(function() {

            // try to find the user based on their google id
            User.findOne({ "google.id" : profile.id }, function(err, user) {
                if (err)
                    return done(err);

                if (user) {

                    // if a user is found, log them in
                    return done(null, user);
                } else {
                    // if the user isnt in our database, create a new user
                    var newUser          = new User();

                    // set all of the relevant information
                    newUser.google.id    = profile.id;
                    newUser.google.token = token;
                    newUser.google.name  = profile.displayName;
                    newUser.google.email = profile.emails[0].value; // pull the first email

                    // save the user
                    newUser.save(function(err) {
                        if (err)
                            throw err;
                        return done(null, newUser);
                    });
                }
            });
        });

    }));


/*
 *
 * Passport Twitter Login configurations
 *
 */ 
passport.use(new TwitterStrategy({

        consumerKey     : oauth_config.twitter.consumerKey,
        consumerSecret  : oauth_config.twitter.consumerSecret,
        callbackURL     : oauth_config.twitter.callbackURL

    },
    function(token, tokenSecret, profile, done) {

    // make the code asynchronous
    // User.findOne won"t fire until we have all our data back from Twitter
        process.nextTick(function() {

            User.findOne({ "twitter.id" : profile.id }, function(err, user) {

                // if there is an error, stop everything and return that
                // ie an error connecting to the database
                if (err)
                    return done(err);

                // if the user is found then log them in
                if (user) {
                    return done(null, user); // user found, return that user
                } else {
                    // if there is no user, create them
                    var newUser                 = new User();

                    // set all of the user data that we need
                    newUser.twitter.id          = profile.id;
                    newUser.twitter.token       = token;
                    newUser.twitter.username    = profile.username;
                    newUser.twitter.displayName = profile.displayName;

                    // save our user into the database
                    newUser.save(function(err) {
                        if (err)
                            throw err;
                        return done(null, newUser);
                    });
                }
            });

      });

    }));


/*
 *
 * Passport Facebook Login configurations
 *
 */ 
passport.use(new FacebookStrategy({

        // pull in our app id and secret from our auth.js file
        clientID        : oauth_config.facebook.clientID,
        clientSecret    : oauth_config.facebook.clientSecret,
        callbackURL     : oauth_config.facebook.callbackURL

    },
    // facebook will send back the token and profile
    function(token, refreshToken, profile, done) {

        // asynchronous
        process.nextTick(function() {

            // find the user in the database based on their facebook id
            User.findOne({ "facebook.id" : profile.id }, function(err, user) {

                // if there is an error, stop everything and return that
                // ie an error connecting to the database
                if (err)
                    return done(err);

                // if the user is found, then log them in
                if (user) {
                    return done(null, user); // user found, return that user
                } else {
                    // if there is no user found with that facebook id, create them
                    var newUser            = new User();

                    // set all of the facebook information in our user model
                    newUser.facebook.id    = profile.id; // set the users facebook id                   
                    newUser.facebook.token = token; // we will save the token that facebook provides to the user                    
                    newUser.facebook.name  = profile.name.givenName + " " + profile.name.familyName; // look at the passport user profile to see how names are returned
                    newUser.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we"ll take the first

                    // save our user to the database
                    newUser.save(function(err) {
                        if (err)
                            throw err;

                        // if successful, return the new user
                        return done(null, newUser);
                    });
                }

            });
        });

    }));

/*
 *
 * Passport Github Login configurations
 *
 */ 
passport.use(new GithubStrategy({

        // pull in our app id and secret from our auth.js file
        clientID        : oauth_config.github.clientID,
        clientSecret    : oauth_config.github.clientSecret,
        callbackURL     : oauth_config.github.callbackURL

    },
    // facebook will send back the token and profile
    function(token, refreshToken, profile, done) {

        // asynchronous
        process.nextTick(function() {

            // find the user in the database based on their facebook id
            User.findOne({ "github.id" : profile.id }, function(err, user) {

                // if there is an error, stop everything and return that
                // ie an error connecting to the database
                if (err)
                    return done(err);

                // if the user is found, then log them in
                if (user) {
                    return done(null, user); // user found, return that user
                } else {
                    // if there is no user found with that facebook id, create them
                    var newUser            = new User();

                    // set all of the facebook information in our user model
                    newUser.github.id    = profile.id; // set the users facebook id                   
                    newUser.github.token = token; // we will save the token that facebook provides to the user                    
                    newUser.github.name  = profile.name.givenName + " " + profile.name.familyName; // look at the passport user profile to see how names are returned
                    newUser.github.email = profile.emails[0].value; // facebook can return multiple emails so we"ll take the first

                    // save our user to the database
                    newUser.save(function(err) {
                        if (err)
                            throw err;

                        // if successful, return the new user
                        return done(null, newUser);
                    });
                }

            });
        });

    }));


// Check if the user is logged in 
function check_login(req, res, next) {
    if (req.session.passport.user) {

        User.findOne({
                _id: new ObjectId(req.session.passport.user)
            }, function (err, user) {

                if (user) {
                    req.current_user = user;

                    // Until regularizing account information is made better, check all of the account information for different social accounts

                    // Check Google
                    if (typeof req.current_user.google.name !== "undefined"){
                        req.current_user.name = req.current_user.google.name;

                    // Check Facebook
                    }else if (typeof req.current_user.facebook.name !== "undefined"){
                        req.current_user.name = req.current_user.facebook.name;

                    // Check Twitter
                    }else if (typeof req.current_user.twitter.name !== "undefined"){
                        req.current_user.name = req.current_user.twitter.name;

                    // Check Github 
                    }else if (typeof req.current_user.github.name !== "undefined"){
                        req.current_user.name = req.current_user.github.name;

                    // Check Local Auth
                    }else if (typeof req.current_user.local.name !== "undefined"){
                        req.current_user.name = req.current_user.local.name;

                    // If can"t find the name, just use "User"
                    }else {
                        req.current_user.name = "User" ;
                    }

                } else {
                    // Not logged in
                    req.current_user = null;

                }
                next();
            });

    } else {
        // Not logged in
        req.current_user = null;
        next();
    }

}


module.exports = {
        // Expose the check_login function
        check_login : check_login
    };


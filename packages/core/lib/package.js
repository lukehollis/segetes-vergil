var path 	= require("path")
,	fs 		= require("fs")
,	Package = function( package_name ){

	this.name = package_name;
	this.package_dir = path.join( path.dirname(require.main.filename), "/packages/" + package_name + "/" );
	this.routes = false;
	this.models = false;
	this.controllers = false;

	this.load = function(){
		console.log( "Server: -- Loading package: " + package_name );

		this._load_routes();
		this._load_models();
		this._load_controllers();

	};

	this._load_routes = function(){

		try {
			this.routes = require( path.join( this.package_dir + "routes.js" ) );
			if ( typeof this.routes !== "undefined" ){
				console.log( "Server: -- -- loaded routes" );
			}
		} catch (e) {
			if ( 
					typeof e.code ==="undefined"
				|| ( 
					typeof e.code !== "undefined" 
						&& e.code !== "MODULE_NOT_FOUND"
					)
				){
					console.log("Server: -- -- error loading routes:", e);
			}
		};

	};

	this._load_models = function(){

		try {
			this.models = require( path.join( this.package_dir + "models" ) );
			if ( typeof this.models !== "undefined" ){
				console.log( "Server: -- -- loaded models" );
			}
		} catch (e) {
			if ( 
					typeof e.code ==="undefined"
				|| ( 
					typeof e.code !== "undefined" 
						&& e.code !== "MODULE_NOT_FOUND"
					)
				){
					console.log("Server: -- -- error loading models:", e);
			}
		};

	};

	this._load_controllers = function(){

		try {
			this.controllers = require( path.join( this.package_dir + "controllers" ) );
			if ( typeof this.controllers !== "undefined" ){
				console.log( "Server: -- -- loaded controllers" );
			}
		} catch (e) {
			if ( 
					typeof e.code ==="undefined"
				|| ( 
					typeof e.code !== "undefined" 
						&& e.code !== "MODULE_NOT_FOUND"
					)
				){
					console.log("Server: -- -- error loading controllers:", e);
			}
		};

	};

};

module.exports = Package;
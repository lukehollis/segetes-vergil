/* 
 * Server.js
 *
 * Application server init and configurations 
 *
 */

var newrelic = require("newrelic")
,	express = require("express")
,	http = require("http")
,	path = require("path")
,	jade = require("jade")
,	passport = require("passport")
,	bodyParser = require("body-parser")
,	cookieParser = require("cookie-parser")
,	session = require("express-session")
,	favicon = require("serve-favicon")
,	flash = require("connect-flash")
,	sitemap = require("express-sitemap")()

,	core_routes = require("../routes")
,	auth = require("../lib/authentication")
,	app_root_dir = path.dirname(require.main.filename)

,	Server = function( segetes_config ){

		var app = express()
		,	db 	= {} 
		,	active_packages = []
		;

		/*
		 * Establish environment
		 * Set Global application variables from configuration file
		 *
		 */
		var app_env = process.env.NODE_ENV = process.env.NODE_ENV || "development";
		var db_engine = segetes_config.db.engine;
		var active_theme = segetes_config.active_theme;
		app.set( "app_env", app_env );
		app.set( "db_engine", db_engine );
		app.set( "site_title", segetes_config.site_title || "Segetes" );
		app.set( "tagline", segetes_config.tagline || "A networked text built with Segetes" );
		app.set( "active_theme", active_theme || "segetes" );

		/*
		 * Load correct ORM for specifed database engine
		 */
		if ( db_engine === "mongo" ){
			var mongoose = require("mongoose");

		}else if ( db_engine === "postgres" ){
			var pg = require("pg"); 

		}

		/*
		 * Environment-specific configurations
		 */
		// Production settings
		if( app_env === "production" ){
			// Set port
			app.set("port", process.env.PORT || 12765);

			// Configure prerender if using prerender 
			segetes_config.prerender.use_prerender = segetes_config.prerender.use_prerender || false;
			if ( segetes_config.prerender.use_prerender ){
				app.use( require("prerender-node").set( "prerenderToken", segetes_config.prerender.token ));
			}

			// Establish databse connection
			if ( db_engine === "mongo" ){
				mongoose.connect( segetes_config.db.mongo[ app_env ].uri, segetes_config.db.mongo[ app_env ].options );

			}else if ( db_engine === "postgres" ) {
				pg.connect( segetes_config.db.postgres[ app_env ].uri, segetes_config.db.postgres[ app_env ].options );

			}

		// Beta settings
		}else if(process.env.NODE_ENV === "beta"){
			// Set port
			app.set("port", process.env.PORT || 22179);

			// Establish database connection
			if ( db_engine === "mongo" ){
				mongoose.connect( segetes_config.db.mongo[ app_env ].uri, segetes_config.db.mongo[ app_env ].options );

			}else if ( db_engine === "postgres" ) {
				pg.connect( segetes_config.db.postgres[ app_env ].uri, segetes_config.db.postgres[ app_env ].options );

			}

		// Development settings
		}else {
			// Set port
			app.set("port", process.env.PORT || 3000);

			// Establish database connection
			if ( db_engine === "mongo" ){
				mongoose.connect( segetes_config.db.mongo[ app_env ].uri, segetes_config.db.mongo[ app_env ].options );
				mongoose.set("debug", true);

			}else if ( db_engine === "postgres" ) {
				pg.connect( segetes_config.db.postgres[ app_env ].uri, segetes_config.db.postgres[ app_env ].options );

			}
		}


		/*
		 * Express Configurations
		 *
		 */
		// Set Favicon
		app.use( favicon( path.join( app_root_dir, "/static/img/favicon.ico" ) ) );

		// Configure static file server
		app.use( express.static( path.join( app_root_dir, "/static" ) ) );

		// Set the view template directories 
		app.set("views", [
							path.join( app_root_dir, "/themes/" + active_theme + "/templates" ), 
							path.join( app_root_dir, "/packages/admin/templates" ), 
							path.join( app_root_dir, "/themes/segetes/templates" ) 
						] );

		// Set the views engine
		app.set( "view engine", segetes_config.view_engine );


		/*
		 * Middleware
		 */
		app.use( cookieParser() );
		app.use( bodyParser.json() );
		app.use( bodyParser.urlencoded({ extended: true }) );

		// User login / session middleware
		app.use( session({ 
			secret: segetes_config.app_session_secret, 
			saveUninitialized: true,
			resave: true
		}) );
		app.use( passport.initialize() );
		app.use( passport.session() );

		// Get the login info
		app.use( auth.check_login );

		/*
		 * Initiate database connection and log errors
		 */
		if ( db_engine === "mongo" ){
			mongoose.connection.on("open", function(err) {
				if (err) {
					console.log(err);
				}else {
				    console.log("Server: Connected to Mongo @", segetes_config.db.mongo[ app_env ].uri );
				}
			});

		}else if ( db_engine === "postgres" ) {
			pg.connection.on("open", function(err) {
				if (err) {
					console.log(err);
				}else {
				    console.log("Server: Connected to PostgreSQL @", segetes_config.db.postgres[ app_env ].uri );
				}
			});

		}

		/*
		 * Initalize core routes list
		 */
		core_routes.initialize( app );

		/*
		 * Load active packages 
		 * -- Core should not be listed in active packages (as it should inherently always be active)
		 */
		console.log( "Server: Configuring active packages" );
		segetes_config.active_packages.forEach(function(package_name){

				// Require the app.js from the active packages
				var active_package = require( path.join( app_root_dir, "/packages/" + package_name + "/app.js" ) );

				// Load the package (routes, models, controllers)
				active_package.load();

				// Ensure the routes are loaded with the app
				if ( "routes" in active_package && active_package.routes !== false ){
					try{
						active_package.routes.initialize( app );
					}catch(e){
						console.log("Server: -- -- could not initalize routes for package:", e);
					}

				}

				// Push the loaded package to the active packages array
				active_packages.push( active_package );

			});

		/*
		 * Handle 404s
		 */

		app.use(function(req, res, next){
			controllers.error_page(req, res);
		});

		// Include the active packages with our app instance
		app.set( "active_packages", active_packages );

		/*
		 * Create the server and listen on specified port
		 */
		http.createServer(app).listen(app.get("port"), function() {
		    console.log("Server: App initialized and listening on port " + app.get("port"));
		});

		/* 
		 * Expose the app via the server
		 */
		this.app = app;

	}
;

module.exports = Server;
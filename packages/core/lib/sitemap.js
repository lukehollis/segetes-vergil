var   sitemap = require("express-sitemap")
/*
* Generating sitemap
*/
,	Sitemap = function(){

		/*
		 * Add dynamic sitemap definition based on routes in the future 
		 */
		sitemap({

			sitemap : "static/sitemap.xml",
			robots : "static/robots.txt",

			map : {
				"/archive":["get"],
				"/about":["get"]
			},
			route : {

			}
		})

		sitemap.generate(app);
		sitemap.toFile();

		console.log("Server: XMLsitemap file written");
	}
;

module.exports = Sitemap;


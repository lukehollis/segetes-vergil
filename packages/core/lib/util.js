var util = {

	// Load models from active packages
	load_models : function(){
		var active_packages = global.segetes.server.app.get("active_packages")
		,	models = {}
		;

		active_packages.forEach(function(active_package){
			if ( active_package.models ){
				for ( model_key in active_package.models ){	
					models[ model_key ] = active_package.models[ model_key ];
				}
			}
		});

		return models;

	},

	// Ensure a user is logged in 
	is_logged_in : function (req, res, next, redirect_url) {

		// Default the redirect_url to index 
		if ( typeof redirect_url === "undefined" ){
			redirect_url = "/";
		}

		// If user is authenticated in the session, carry on
		if ( req.is_authenticated() ){
			return next();
		}

		// If the user is not logged in, redirect them to url
		res.redirect( redirect_url );
	},

	// Input string to slug
	slugify : function ( string ) {

		return text.toString().toLowerCase()
					.replace(/\s+/g, "-")		// Replace spaces with -
					.replace(/[^\w\-]+/g, "")   // Remove all non-word chars
					.replace(/\-\-+/g, "-")		// Replace multiple - with single -
					.replace(/^-+/, "")			// Trim - from start of text
					.replace(/-+$/, "");		// Trim - from end of text
	}

};

module.exports = util;
var mongoose	= require("mongoose")
,	Schema 		= mongoose.Schema
;


var linkedAppSchema  = new Schema ({
		title : String,
		url : String
	},{
		collection : "linked_apps"
	});

var cacheSchema  = new Schema ({
		key : String,
		value : String
	},{
		collection : "cache"
	});


module.exports = {
		linked_apps : mongoose.model("LinkedApp", linkedAppSchema),
		cache : mongoose.model("Cache", cacheSchema),
	};	

/* 
 * Routes.js
 *
 * Routes for the application 
 *
 */

var controllers = require("./controllers")
,   auth = require("./lib/authentication")
,   passport = require("passport")
;  


module.exports.initialize = function(app) {

	/*
	 * Data rest api 
	 */

	app.post("/api/:query?*", controllers.query);
	app.get("/api/?*", controllers.query);


	/*
	 * Login / Authentication
	 */

	// Local
	app.post("/auth/local", passport.authenticate( "local-login", {
			successRedirect : "/",
			failureRedirect : "/"
		}));
	app.post("/auth/local/signup", passport.authenticate( "local-signup", {
			successRedirect : "/",
			failureRedirect : "/"
		}));

	// Google
	app.get("/auth/google", passport.authenticate("google", { scope : ["profile", "email"] }));
	app.get("/auth/google/callback",
			passport.authenticate("google", {
					successRedirect : "/",
					failureRedirect : "/"
			}));

	// Twitter
	app.get("/auth/twitter", passport.authenticate("twitter"));
	app.get("/auth/twitter/callback",
		passport.authenticate("twitter", {
			successRedirect : "/",
			failureRedirect : "/"
		}));

	// Facebook
	app.get("/auth/facebook", passport.authenticate("facebook", { scope : "email" }));
	app.get("/auth/facebook/callback",
		passport.authenticate("facebook", {
			successRedirect : "/",
			failureRedirect : "/"
		}));

	// Github 
	app.get("/auth/github", passport.authenticate("github", { scope : "email" }));
	app.get("/auth/github/callback",
		passport.authenticate("github", {
			successRedirect : "/",
			failureRedirect : "/"
		}));
	app.post("/auth/check", controllers.auth_check);

	// Logout
	app.get("/logout", function(req, res) {
		req.logout();
		res.redirect("/");
	});


	/*
     * Search
     */

	app.get("/search", controllers.search);


	/*
	 * Index
	 */

	app.get("/", controllers.index);

    
	
};


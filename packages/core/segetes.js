var segetes_config = require("../../segetes_config")
,	Server = require("./lib/server")
,	Segetes = function(){

	this.config = segetes_config;
	this.server = {};


	this.serve = function(){

		this.server = new Server( segetes_config );

	};


};

module.exports = Segetes;
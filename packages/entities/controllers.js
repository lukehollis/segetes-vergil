var controllers = {

	list : function(req, res) {
		res.render("archive", {
			user : req.current_user,
			page_class : "archive entity-archive", 
			model : "entities",
			title : "Entities",
			page_title : "Entities"
		});
	},

	single : function(req, res) {
		res.render("entities/single", {
			user : req.current_user,
			page_class : "entity_single", 
			page_title : "Entity"
		});
	}

};

module.exports = controllers;

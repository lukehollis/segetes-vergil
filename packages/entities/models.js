var mongoose 			= require("mongoose")
,	Schema 				= mongoose.Schema
;

var entitySchema  = new Schema ({
		en_name : String,
		l_names : [String],
		slug : String,
		lines : [String],
		thumbnail : String,
		desc : String,
		works : [String],
		link : String
	},{
		collection : "entities"
	}); 


module.exports = {
		entities : mongoose.model("Entity", entitySchema),
	};	

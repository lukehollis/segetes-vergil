var controllers = require("./controllers")
;

var	routes = {

	initialize : function( app ){


		app.get("/entities", controllers.list);

		app.get("/entities/:entity_query?*", controllers.single);

	}
 
};

module.exports = routes; 
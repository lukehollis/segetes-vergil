var nodemailer = require("nodemailer")
, 	controllers = {

	list: function(req, res) {
		res.render("archive", {
			user : req.current_user,
			page_class : "archive media-archive", 
			model : "media",
			title : "Media",
			page_title : "Media"
		});
	},

	single : function(req, res) {
		res.render("media/single", {
			user : req.current_user,
			page_class : "media_single", 
			page_title : "Media"
		});
	},

	send_modal : function(req, res){

		var transporter = nodemailer.createTransport({
				service: "Gmail",
				auth: {
					user: "lukehollis@gmail.com",
					pass: "Sidereterram1212!"
				}
			})
		,   mailOptions = {
				from: "Luke Hollis <luke@segetes.io>",
				to: "lukehollis@gmail.com",
				subject: "Media Upload on Segetes",
				text: JSON.stringify({
					body : req.body,
					user : req.current_user  
				 })
			};

		// send mail with defined transport object
		transporter.sendMail(mailOptions, function(error, info){
			if(error){
				console.log(error);
			}else{
				console.log("Message sent: " + info.response);
			}
		});

		res.send(true);

	},

};

module.exports = controllers;

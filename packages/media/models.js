var mongoose 	= require("mongoose")
,	Schema 		= mongoose.Schema
,   ObjectId	= Schema.ObjectId
;

var mediaSchema  = new Schema ({
		title : 	String,
		slug : 		String,
		thumbnail : String,
		lines : 	[String],
		link : 		String,
		desc : 		String,
		works : 	[String],
		video : 	String,
		audio : 	String
	},{
		collection : "media"
	}); 

module.exports = {
		media : mongoose.model("Media", mediaSchema),
	};	

var controllers = require("./controllers")
;

var	routes = {

	initialize : function( app ){

		app.get("/media", controllers.list );

		app.get("/media/:media_query", controllers.single );
		
		// Send modal data 
		app.post("/send_modal", controllers.send_modal);

	}
 
};

module.exports = routes; 
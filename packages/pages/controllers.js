var controllers = {

	about : function(req, res) {
		res.render("pages/about", {
			user : req.current_user,
			page_class : "about", 
			page_title : "About"});
	},

	terms : function(req, res) {
		res.render("pages/terms", {
			user : req.current_user,
			page_class : "terms", 
			page_title : "Terms of Use and Privacy Policy"});
	},

	sources : function(req, res) {
		res.render("pages/sources", {
			user : req.current_user,
			page_class : "sources", 
			page_title : "Sources"});
	},
	
	dcne_pres : function(req, res) {
		res.render("pages/dcne_pres", {
			user : req.current_user,
			page_class : "presentation", 
			page_title : "Segetes, Digital Classicist New England, 2015"
		});
	},

};

module.exports = controllers;

{
  "name": "texts",
  "version": "0.1.0",
  "description": "Handling text-based features for the Segetes framework",
  "main": "app.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "author": "Segetes",
  "license": "MIT"
}

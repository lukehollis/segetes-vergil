var controllers = require("./controllers")
;

var	routes = {

	initialize : function( app ){

		app.get("/about", controllers.about);
		app.get("/terms", controllers.terms);
		app.get("/sources", controllers.sources);
		app.get("/dcne2015", controllers.dcne_pres);

	}
 
};

module.exports = routes; 

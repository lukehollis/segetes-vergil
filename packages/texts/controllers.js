var controllers = {

	author : function(req, res) {
		res.render("texts/author", {
			user : req.current_user,
			page_class : "author", 
			page_title : "Author"
		});
	},
	list : function(req, res) {
		res.render("texts/list", {
			user : req.current_user,
			page_class : "text_index", 
			page_title : "Texts"
		});
	},
	text : function(req, res) {
		res.render("texts/text", {
			user : req.current_user,
			page_class : "text", 
			page_title : ""
		});
	},

};

module.exports = controllers;

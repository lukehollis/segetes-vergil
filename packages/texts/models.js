var mongoose 			= require("mongoose")
,	Schema 				= mongoose.Schema
,   ObjectId			= Schema.ObjectId
;

var lineSchema 	= new Schema ({
		author : String,
		text : String,
		work : String,
		book : String
	},{
		collection : "lines"
	}); 

var authorSchema  = new Schema ({
		urn_code : String,
		works : [String]
	},{
		collection : "authors"
	});

var workSchema  = new Schema ({
		urn_code : String,
		authors : [String],
		subworks : [String]
		
	},{
		collection : "works"
	});


var translationSchema  = new Schema ({
		work : String,
		edition : {
				translators : [ String ],
				slug : String
			},
		line : {
				text : String,
				n : Number
			},
		subwork : {
				n : Number
			},
		orig_l_ns : [ Number ]
	},{
		collection : "translations"
	}); 

module.exports = {
		lines : mongoose.model("Line", lineSchema),
		authors : mongoose.model("Author", authorSchema),
		works : mongoose.model("Work", workSchema),
		translations : mongoose.model("Translation", translationSchema)
	};	





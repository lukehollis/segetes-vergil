var controllers = require("./controllers")
;

var	routes = {

	initialize : function( app ){

		/*
		 * Single text routes
		 *
		 *  - Query by book name, subwork (poem or book number), line(s)
		 *
		 */
	    app.get("/author/:author", controllers.author);
		app.get("/texts", controllers.list);
		app.get("/:work_query?*", controllers.text);

	}
 
};

module.exports = routes; 
var controllers = require("./controllers")
;

var	routes = {

	initialize : function( app ){

		/*
		 * URN redirects
		 */
		app.get("/urn/?*", controllers.urn_resolver);

	}
 
};

module.exports = routes; 
var urns = {
		"phi0690" : {
			"slug" : "vergil",
			"works" : {
				"phi003" : {
					"slug" : "aeneid"
				},	
				"phi002" : {
					"slug" : "georgics"
				},
				"phi001" : {
					"slug" : "eclogues"
				}
			}
		}
	}

,	resolver = function( req, res ) {
	
		var path 	= req.path.split("/") 
		,	urn 	= []
		,	text 	= []
		,	in_text = []
		,	lines 	= []
		,	work 	= ""
		,	subwork = 1
		,	l_n_f 	= 1
		,	l_n_t	= 0
		,	url		= "/"
		;	


		if ( path.length > 2 ){
			urn = path[2].split(":");

			// Parse the text (work/subwork)
			if ( urn.length > 3 ){
				text = urn[3].split(".");

				if( text.length > 1 && typeof urns[ text[0] ] !== "undefined" ){

					author = urns[ text[0] ];

					if ( typeof author.works[ text[1] ] !== "undefined" ){

						work = author.works[ text[1] ];

						// Parse the in text address
						if ( urn.length > 4 ) {
							in_text = urn[4].split(".");

							if ( in_text.length > 0 ){
								subwork = in_text[0];
							}
							if ( in_text.length > 1 ){
								lines = in_text[1].split("-");
								if ( lines.length > 0 ){
									l_n_f = lines[0];
								}
								if ( lines.length > 1 ){
									l_n_t = lines[1];
								}
							}
						}

						// Construct URL to redirect to 
						url = url + work.slug + "/" + subwork + "/" + l_n_f;
						if ( l_n_t !== 0 ){
							url = url + "-" + l_n_t;
						}
						console.log("URN Redirect:", urn, url);
						res.redirect( url );
					}
				}

			}else {
				res.send("URN not found");
			}


		}else {
			res.send("URN not found");

		}

	}
;


module.exports = resolver;
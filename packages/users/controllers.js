var controllers = {

	signup : function(req, res) {
		res.render("signup", {
			user : req.current_user,
			page_class : "signup", 
			page_title : "Create Account"
		});
	},

	user : function(req, res) {
		var title = ""
		;
		if ( req.current_user !== null ){
			title = req.current_user.name + title;
		}

		res.render("profile", {
			page_class : "profile", 
			user : req.current_user,
			page_title : title 
		});
	},

	save_note : function(req, res) {
		user_functions(req, res)
			.save_note();
	},

	update_note : function(req, res) {
		user_functions(req, res)
			.update_note();
	},

	remove_note : function(req, res) {
		user_functions(req, res)
			.remove_note();
	},
	
	fetch_notes : function(req, res) {
		user_functions(req, res)
			.fetch_notes();
	},

};

module.exports = controllers;

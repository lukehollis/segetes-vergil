/* 
 * Users.js
 *
 * User account functions and management  
 *
 */

var models = require("../models")
,	Note = models.notes 
,	Upload = models.uploads
,	mongoose = require("mongoose")
,	ObjectId = mongoose.Schema.ObjectId	
,	note = {}
,	upload = {}
,	query
,	user_functions = function(req, res){

		this.save_note = function(){
		// Save a note posted in the request body
			if (this.process_note_params()){
				var newNote = new Note();

				newNote.text = req.body.text;
				newNote.ref = req.body.ref;
				newNote.user = req.current_user._id;

                // save the note 
                newNote.save(function(err) {
                    if (err)
                        throw err;
                    return res.send(newNote._id); 
                });
			}else {
                return res.send("Error with the note params;"); 
            }
		};

		this.update_note = function(){
		// Save a note posted in the request body
			var query_params  = {}
			,	update_params = {}
			;
			if (this.process_note_params()){

				// Query parameters
				query_params["ref.work"] = req.body.ref.work;
				query_params["ref.subwork.n"] = req.body.ref.subwork.n;
				query_params["ref.line.n"] = req.body.ref.line.n;

				// params to update in the db
				update_params.text = req.body.text;
				update_params.ref = req.body.ref;
				update_params.user = req.current_user._id;

                // update the note 
                Note.update(query_params, update_params, function(err){
                	if(err)
                		throw err;
                	return res.send(true);
                });


			}else {
                return res.send("Error with the note params;"); 
            }
		};

		this.remove_note = function(){
		// Delete the note based on query parameters in the request body
			var query_params  = {}
			,	update_params = {}
			;
			if (this.process_note_params()){

				// Query parameters
				query_params["ref.work"] = req.body.ref.work;
				query_params["ref.subwork.n"] = req.body.ref.subwork.n;
				query_params["ref.line.n"] = req.body.ref.line.n;

				Note.remove(query_params, function(err){
                	if(err)
                		throw err;
                	return res.send(true);
                });

			}else {
                return res.send("Error with the note params;"); 
            }

		};

		this.fetch_notes = function(){
		// Fetch the notes for the select passages
			var find_notes_params = {};
			if (this.process_note_params()){
				find_notes_params = req.body;
				find_notes_params.user = req.current_user._id; 
				query = Note.find(find_notes_params);
				query.limit(100);
				query.exec( this.send_response );
			}else {
                return res.send("Error with the note params;"); 
            }
		};

		this.process_note_params = function(){
		// Process the note params
			var flag = true;
			if ( req.current_user === null ){
				flag = false;
			}
			if ( flag && typeof req.body.ref !== "undefined" ){
				req.body.ref.subwork.n = parseInt(req.body.ref.subwork.n);
				req.body.ref.line.n = parseInt(req.body.ref.line.n);
			}

			return flag;
		};

		this.save_upload = function(){
		// Save an upload from a user upload

		};

		this.delete_upload = function(){
		// Delete an upload

		};

		this.send_response = function(err, results) {

			// set correct headers
			res.header("Access-Control-Allow-Origin", "http://localhost");
			res.header("Access-Control-Allow-Methods", "GET, POST");
			res.header("Content-Type", "application/json");

			res.json( results );
		}

		return this;
	}

;
module.exports = user_functions;
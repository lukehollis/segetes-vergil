var mongoose 			= require("mongoose")
,	Schema 				= mongoose.Schema
,   ObjectId			= Schema.ObjectId
,	bcrypt				= require("bcryptjs")
,	SALT_WORK_FACTOR	= 10
;

var noteSchema  = new Schema ({
		ref : {
				work : String,
				subwork : {
					n : Number
				},
				line : {
					n : Number
				}
			},
		user : ObjectId,
		text : String
	},{
		collection : "notes"
	});

var uploadSchema  = new Schema ({
		user : ObjectId,
		title : String,
		slug : String,
		lines : [String],
		link : String,
		type : String,
		desc : String
	},{
		collection : "uploads"
	});

var userSchema  = new Schema ({

		username : String,
		email : String, 

		local : {
			name 		: String,
			email		: String,
			password 	: String
		},

		facebook : {
			id 			: String,
			token		: String,
			email		: String,
			name 		: String
		},

		twitter : {
			id 			: String,
			token		: String,
			displayName : String,
			username	: String
		},

		google : {
			id 			: String,
			token		: String,
			email		: String,
			name 		: String
		},

		github : {
			id			: String,
			token		: String,
			email		: String,
			name		: String
		}

	},{
		collection : "users"
	}); 


// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
	return bcrypt.compareSync(password, this.local.password);
};

// Add a compare password method
userSchema.methods.comparePassword = function(input_pass, callback) {
	bcrypt.compare(input_pass, this.password, function(err, is_match) {
		if (err) return callback(err);
		callback(null, is_match);
	});
};

module.exports = {
		notes : mongoose.model("Note", noteSchema),
		uploads : mongoose.model("Upload", uploadSchema),
		users : mongoose.model("User", userSchema),
	};	

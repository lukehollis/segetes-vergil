var controllers = require("./controllers")
,	is_logged_in = require("../core/lib/util").is_logged_in
;

var	routes = {

	initialize : function( app ){

		// Notes
		app.post("/save_note", is_logged_in, controllers.save_note);
		app.post("/update_note", is_logged_in, controllers.update_note);
		app.post("/remove_note", is_logged_in, controllers.remove_note);
		app.post("/fetch_notes", is_logged_in, controllers.fetch_notes);

		// Profile
		app.get("/profile", is_logged_in, controllers.user);

		// Signup
		app.get("/signup", controllers.signup);

	}
 
};

module.exports = routes; 
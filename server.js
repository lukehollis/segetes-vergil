
var Segetes = require("./packages/core/segetes")
,	app = new Segetes()
;

// Set global segetes application for use in other packages and modules
global.segetes = app;

// Serve the app 
global.segetes.serve();

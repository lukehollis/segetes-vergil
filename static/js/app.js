/*
 * Initialize the angular application 
 * with primary module aw
 */

angular.module('sg', ['sgFilters', 'ngSanitize', 'headroom']);


angular.module('sgFilters', [])
	.filter('capitalize', function() {
		return function(input, all) {
			return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
		}
	})
	.filter('slugify', function() {
		return function(input, all) {
			return input 
			    .toLowerCase()
			    .replace(/[^\w ]+/g,'')
			    .replace(/ +/g,'-')
			    ;
		}
	})
	.filter('thumb_small', function() {
		return function(input, all) {
			return (!!input) ? input.replace( ".", "-small.") : '';
		}
	})
	.filter('thumb_large', function() {
		return function(input, all) {
			return (!!input) ? input.replace( ".", "-large.") : '';
		}
	})
	.filter('thumb_large_uncropped', function() {
		return function(input, all) {
			return (!!input) ? input.replace( ".", "-large-uncropped.") : '';
		}
	})
	.filter('roman_numeral', function() {
		return function( n, all){
			var l = "";
			if ( n === 1 ) {
				l = "I";
			}else if ( n === 2 ){
				l = "II";
			}else if ( n === 3 ){
				l = "III";
			}else if ( n === 4 ){
				l = "IV";
			}else if ( n === 5 ){
				l = "V";
			}else if ( n === 6 ){
				l = "VI";
			}else if ( n === 7 ){
				l = "VII";
			}else if ( n === 8 ){
				l = "VIII";
			}else if ( n === 9 ){
				l = "IX";
			}else if ( n === 10 ){
				l = "X";
			}else if ( n === 11 ){
				l = "XI";
			}else if ( n === 12 ){
				l = "XII";
			}
			return l;
		}

	})
	.filter('from_roman_numeral', function() {
		return function( title, all){
			var l = ""
			,	n = 1	
			;

			title = title.split(" ");
			if( title.length > 0 ){
				n = title[0];
			}

			if ( n === "I" ) {
				l = 1;
			}else if ( n === "II" ){
				l = 2;
			}else if ( n === "III" ){
				l = 3;
			}else if ( n === "IV" ){
				l = 4;
			}else if ( n === "V" ){
				l = 5;
			}else if ( n === "VI" ){
				l = 6;
			}else if ( n === "VII" ){
				l = 7;
			}else if ( n === "VIII" ){
				l = 8;
			}else if ( n === "IX" ){
				l = 9;
			}else if ( n === "X" ){
				l = 10;
			}else if ( n === "XI" ){
				l = 11;
			}else if ( n === "XII" ){
				l = 12;
			}
			return l;
		}

	})
	.filter('unsafe', function($sce) { 
		return $sce.trustAsHtml; 
	});

 

/*angular.module('aw').config(['$stateProvider',
  function($stateProvider) {
    $stateProvider.state('index', {
      url: '/',
      templateUrl: '../templates/index.jade'
    });
  }
]);
*/
/*
 *
 * Site.js - site specific functions for the aweb portfolio
 *
 */

(function($){
angular.element(document).ready(function() {

window.__sg__ = window.__sg__ || {};
var Sg = window.__sg__;

// fix iOS font related issues 
if(navigator.userAgent.match(/iPad/i)){
	$("html").addClass("ipad");
}

// js - modernizer
$('.no-js').removeClass('no-js').addClass('js');

Sg.close_filler_modal = function() {
  $("#filler_modal").addClass("hidden");
}
//if (document.cookie.indexOf("segetesBetaModal") < 0) {
	//document.cookie = "segetesBetaModal=true; expires=Fri, 2 Feb 2015 23:59:59 GMT; path=/";
	//$("#filler_modal").removeClass("hidden");
//}

$(".close-link").on("click", function(){
	Sg.close_filler_modal();	
});

/*
 * Scroll interactions for index
 */

// establish scroll variables
Sg.curr_depth = $(window).scrollTop();
Sg.prev_depth = 0;
Sg.winHeight = window.innerHeight ? window.innerHeight : $(window).height();
Sg.target_depth = 0;
Sg.header_hidden = false;
Sg.with_meta_lines = [];

// Scroll event
$(document).on("scroll touchstart", function(e) {

	Sg.prev_depth = Sg.curr_depth;
	Sg.curr_depth = $(window).scrollTop();

	// Down scroll event
	if(Sg.prev_depth < Sg.curr_depth){

		if(Sg.curr_depth > Sg.target_depth){
			//Sg.Lib.toggle_nav_fix($target, 'fix');

			// Toggle the header hidden information
			if (Sg.header_hidden == false){
				Sg.header_hidden = true;
			}

		}

		// Regulate the fixed lines on the single page
		do_fixed_lines();

	// Up scroll event
	}else {

		if(Sg.curr_depth < Sg.target_depth){
			//Sg.Lib.toggle_nav_fix($target, 'unfix');

			if (Sg.header_hidden == true){
				Sg.header_hidden = false;
			}

		}

		// Regulate the fixed lines on the single page
		do_fixed_lines();

	}
});

function do_fixed_lines(){


	// Check for sticky lines
	// Set the old lemma to compare against the new
	if (typeof Sg.lemma_fixed_n !== "undefined" ){
		Sg.old_lemma_fixed_n = Sg.lemma_fixed_n;
	}

	// Get the element with the highest offset that is greater than the current offset
	Sg.with_meta_lines.forEach(function(e){
		if( e.h < Sg.curr_depth ){
			Sg.lemma_fixed_n = e.n;
			Sg.lemma_fixed_h = e.h;
		}else if ( e.n === Sg.lemma_fixed_n ){
			// If the current offset is greater than the lemma, mark it to be unfixed 
			if (Sg.lemma_fixed_h > Sg.curr_depth){
				Sg.lemma_fixed_n_to_unfix = e.n;
			}
		}
	});

	// If there's an old lemma that doesn't equal the current lemma, remove it. 
	if ( typeof Sg.old_lemma_fixed_n !== "undefined" ){
		if ( Sg.old_lemma_fixed_n !== Sg.lemma_fixed_n ){
			$(".line[data-n='" + Sg.old_lemma_fixed_n + "'] .lemma").removeClass("lemma-fixed");
		}
	}

	// If we have a lemma to fix
	if ( typeof Sg.lemma_fixed_n !== "undefined" ){
		// If we have a lemma to unfix and it's the currently fixed lemma
		if ( typeof Sg.lemma_fixed_n_to_unfix !== "undefined" && Sg.lemma_fixed_n === Sg.lemma_fixed_n_to_unfix ) {

			$(".line[data-n='" + Sg.lemma_fixed_n + "'] .lemma").removeClass("lemma-fixed");
			Sg.lemma_fixed_n_to_unfix = 0;

		// Otherwise, fix the lemma
		}else {
			$(".line[data-n='" + Sg.lemma_fixed_n + "'] .lemma").addClass("lemma-fixed");
		}
	}
}

/*
 *  Side Menu 
 */
Sg.side_menu_shown = false;
$("body").on("click", function(e){
	var $target = $(e.target)
	,	$panel = $(".side-menu-panel")
	;
	console.log($target);

	// Toggle the side menu if click on side-menu-head 
	if ( 
			$target.parents(".side-menu").length > 0 
		|| $target.parents(".side-menu-head").length > 0 
		|| $target.hasClass("side-menu") === true 
		){ 

		$('.side-menu-panel').toggleClass('shown');
		if ( Sg.side_menu_shown ){
			Sg.side_menu_shown = false;
		}else{
			Sg.side_menu_shown = true;
		}

	// Else if it's a click on the side menu interior, leave open
	} else if ( 
			$target.parents(".side-menu-panel").length > 0
		|| $target.hasClass("side-menu-panel") === true 
		){
		// Do nothing

	// Close if click not on .side-menu-head
	} else {
		if ( Sg.side_menu_shown ){
			Sg.side_menu_shown = false; 
			$('.side-menu-panel.shown').removeClass('shown');
		}
	}

	// If this is a click on the body, close the text-browser panels
	if (
			$target.parents(".tool-browser").length === 0 
		){
			$(".tool-panel").addClass("hidden");
	}

	if ( $target.hasClass("login") || $target.parents(".login").length > 0 ){
		$(".modal-login").addClass("modal-lowered");
		e.preventDefault();
	}


});


// Login controls 
$(".modal-login").on("click", function(e){
	var $target = $(e.target)
	;
	if($target.parents(".modal-inner").length == 0){
		e.preventDefault();
		$(".modal-login").removeClass("modal-lowered");
	}
});

// Detect Mobile
Sg.is_mobile = window.innerWidth < 600;

});
})(jQuery);

angular.module('sg')
	.controller('ArchiveController', ['$scope', '$http', function($scope, $http) {

	var Sg = window.__sg__
	,	db = Sg.Db
	,	model = $(".ng-archive").data().model
	,	params = {
			model : model, 
			__resp__ : 'items',
			sort : { 'en_name' : 1, 'title' : 1 },
			limit : 21
		}
	,	path = location.pathname.split("/")
	;

	$scope.items = [];
	$scope.model = model;
	Sg.pg = 0;


	$scope.$watch(
		function () { return document.getElementById('items').innerHTML },
		function( value ){
			var val = value || null;

			if ( val ){

				$(".more").removeClass("hidden");
				$(".loading").addClass("hidden");
				$("#loading_modal").fadeOut("fast");

			}

		});

	$scope.$watch(
		function () { return document.getElementById('subnav').innerHTML },
		function( value ){
			var val = value || null;

			if ( val ){

				if ( location.hash.length > 0  && $(".selected").length === 0 ){
					
					$(".subnav-link[data-n='" + location.hash.replace("#", "") + "'").addClass("selected");
				}

			}
		});

	
	if ( model === "something_that_needs_an_alphabet_subnav" ){
		$scope.subnav = [	{
								'title' : "a",
								'slug' : "a"
							},
							{
								'title' : "b",
								'slug' : "b"
							},
							{
								'title' : "c",
								'slug' : "c"
							},
							{
								'title' : "d",
								'slug' : "d"
							},
							{
								'title' : "e",
								'slug' : "e"
							},
							{
								'title' : "f",
								'slug' : "f"
							},
							{
								'title' : "g",
								'slug' : "g"
							},
							{
								'title' : "h",
								'slug' : "h"
							},
							{
								'title' : "i",
								'slug' : "i"
							},
							{
								'title' : "j",
								'slug' : "j"
							},
							{
								'title' : "k",
								'slug' : "k"
							},
							{
								'title' : "l",
								'slug' : "l"
							},
							{
								'title' : "m",
								'slug' : "m"
							},
							{
								'title' : "n",
								'slug' : "n"
							},
							{
								'title' : "o",
								'slug' : "o"
							},
							{
								'title' : "p",
								'slug' : "p"
							},
							{
								'title' : "q",
								'slug' : "q"
							},
							{
								'title' : "r",
								'slug' : "r"
							},
							{
								'title' : "s",
								'slug' : "s"
							},
							{
								'title' : "t",
								'slug' : "t"
							},
							{
								'title' : "u",
								'slug' : "u"
							},
							{
								'title' : "v",
								'slug' : "v"
							},
							{
								'title' : "w",
								'slug' : "w"
							},
							{
								'title' : "x",
								'slug' : "x"
							},
							{
								'title' : "y",
								'slug' : "y"
							},
							{
								'title' : "z",
								'slug' : "z"
							}
						];
	}else {
		$scope.subnav = [	{
								'title' : "Eclogues",
								'slug' : "eclogues"
							},
							{
								'title' : "Georgics",
								'slug' : "georgics"
							},
							{
								'title' : "Aeneid",
								'slug' : "aeneid"
							}
						];
	}


	$scope.more = function( e ){
		$(".more").addClass("hidden");
		$(".loading").removeClass("hidden");

		Sg.pg = Sg.pg + 1;
		params.__bind_method__ = "append";
		$scope.update();

	};

	$scope.filter = function( e ){
		var $target = [] 
		,	f		= '' 
		;

		$("#loading_modal").fadeIn("fast");

		if ( typeof e !== "undefined" ){
			$target = $( e.target );
			f = $target.data().n
			if ( $target.hasClass("selected") ){
				console.log("foo");
				f = ''; 
				location.hash = ''; 
				$target.removeClass("selected");
				delete params.works;
			}else {
				$(".selected").removeClass("selected");
				$target.addClass("selected");
				params.works = f;
				location.hash = f;
			}
		}else {
			f = location.hash.replace("#", "")
			//$(".subnav-link[data-n='" + f + "'").addClass("selected");
			params.works = f;
		}


		Sg.pg = 0;
		params.__bind_method__ = "";

		$scope.update();

	};

	$scope.update = function( e ){

		params.offset = Sg.pg * params.limit; 
		db.query($scope, $http, params);

	};

	if ( location.hash.length > 0 ){
		$scope.filter();
	}else {
		$scope.update();
	}

}]);
angular.module('sg')
	.controller('IndexController', ['$scope', '$http', function($scope, $http) {

		var Sg = window.__sg__
		,	db = Sg.Db
		,	params = {
					'model' : 'lines',
					'work' : 'georgics',
					'subwork.n' : 1,
					'line.n' : 1
			}
		;

		$scope.authors = [{
			'display_name' : 'Vergil',
			'guid' : '/author/vergil',
			'texts' : [
				{
					'title' : 'Aeneid',
					'guid' : '/aeneid/',
					'meta_elements' : {
						'lines' : 9896,
						'entities' : 170,
						'media' : 35,
						'related_passages' : 6066,
						'articles' : 31730,
						'commentary' : 9963 
					},
					'subworks' : [
						{
							'title' : 'I',
							'guid' : '/aeneid/1/' 
						},
						{
							'title' : 'II',
							'guid' : '/aeneid/2/' 
						},
						{
							'title' : 'III',
							'guid' : '/aeneid/3/' 
						},
						{
							'title' : 'IV',
							'guid' : '/aeneid/4/' 
						},
						{
							'title' : 'V',
							'guid' : '/aeneid/5/' 
						},
						{
							'title' : 'VI',
							'guid' : '/aeneid/6/' 
						},
						{
							'title' : 'VII',
							'guid' : '/aeneid/7/' 
						},
						{
							'title' : 'VIII',
							'guid' : '/aeneid/8/' 
						},
						{
							'title' : 'IX',
							'guid' : '/aeneid/9/' 
						},
						{
							'title' : 'X',
							'guid' : '/aeneid/10/' 
						},
						{
							'title' : 'XI',
							'guid' : '/aeneid/10/' 
						},
						{
							'title' : 'XII',
							'guid' : '/aeneid/12/' 
						}
					]
				},
				{
					'title' : 'Georgics',
					'guid' : '/georgics/',
					'meta_elements' : {
						'lines' : 2188,
						'entities' : 83,
						'media' : 10,
						'related_passages' : 994,
						'articles' : 7219,
						'commentary' : 0 
					},
					'subworks' : [
						{
							'title' : 'I',
							'guid' : '/georgics/1/' 
						},
						{
							'title' : 'II',
							'guid' : '/georgics/2/' 
						},
						{
							'title' : 'III',
							'guid' : '/georgics/3/' 
						},
						{
							'title' : 'IV',
							'guid' : '/georgics/4/' 
						}
					]
				},
				{

					'title' : 'Eclogues',
					'guid' : '/eclogues/',
					'meta_elements' : {
						'lines' : 830,
						'entities' : 53,
						'media' : 9,
						'related_passages' : 349,
						'articles' : 5366,
						'commentary' : 0 
					},
					'subworks' : [
						{
							'title' : 'I',
							'guid' : '/eclogues/1/' 
						},
						{
							'title' : 'II',
							'guid' : '/eclogues/2/' 
						},
						{
							'title' : 'III',
							'guid' : '/eclogues/3/' 
						},
						{
							'title' : 'IV',
							'guid' : '/eclogues/4/' 
						},
						{
							'title' : 'V',
							'guid' : '/eclogues/5/' 
						},
						{
							'title' : 'VI',
							'guid' : '/eclogues/6/' 
						},
						{
							'title' : 'VII',
							'guid' : '/eclogues/7/' 
						},
						{
							'title' : 'VIII',
							'guid' : '/eclogues/8/' 
						},
						{
							'title' : 'IX',
							'guid' : '/eclogues/9/' 
						},
						{
							'title' : 'X',
							'guid' : '/eclogues/10/' 
						}
					]
				}
			]
		}];

		// Watch the line update and perform necessary post line update fns
		$scope.$watch(
			function () { return document.getElementById('demo-lines').innerHTML },
			function( value ){
			// Update to the DOM has occured;
				var val = value || null;

				if ( val && $(".line").length > 0 ){

					$scope.update_overflow();
					document.title = "Home | Segetes";

				}

			});

		// Do the inital query parsing the path values
		db.query( $scope, $http, params);



		// Show/hide the meta information for the lines
		$scope.toggle_meta = function(e){
			var $target = $(e.target)
			,	$line 	= $target.parents(".line")
			,	$lines  = $(".line")
			;

			// Toggle the meta information based on all the lines
			if($line.hasClass("with-meta")){

				$line.removeClass("with-meta");
				if(!($lines.hasClass("with-meta"))){
					$lines.removeClass("extended");
				}

			} else {

				$line.addClass("with-meta");

				if(!($lines.hasClass("extended"))){
					$lines.addClass("extended");
				}
			}

		};

		// Update the selected line depths each time height is changed 
		$scope.update_meta_line_depths = function(){
			Sg.with_meta_lines.forEach(function(e){
					e.h = $(".line[data-n='" + e.n + "']").offset().top;
				});
		};

		// Show/hide the selected meta section
		// Sections: definitions, comments, articles, related passages
		$scope.toggle_meta_section = function(e){
			var $target  = $(e.target)
			,	$line 	 = $target.parents(".line")
			,	$section = $line.children(".meta").children(".meta-section." + $target.data().type)
			;

			$target.toggleClass("selected");
			$section.toggleClass("meta-selected");
			$scope.update_meta_line_depths();


		};

		// Show/hide the scansion highlighting
		$scope.toggle_scansion = function(e){
			var $target = $(e.target)
			,	$line 	= $target.parents(".line")
			;
			$target.toggleClass("selected");
			$line.toggleClass("show-scansion");

		};

		$scope.update_overflow = function(){
			// Check commentary overflows
			$(".commentary .meta-item").each( function( k, elem ){
				if( !is_overflowed_h( elem ) && !$( elem ).hasClass("view-more")){
					$( elem ).children(".show-more-toggle").hide();
				}
			} );

			// Check definitions overflows
			$(".definitions .meta-item").each( function( k, elem ){
				if( !is_overflowed_h( elem ) && !$( elem ).hasClass("view-more") ) {
					$( elem ).children(".show-more-toggle").hide();
				}
			} );
		};

		$scope.show_more = function( e ){

			var $target = $(e.target)
			,	$parent = $target.parents(".meta-item")
			;

			if ( !$target.hasClass("show-more-toggle") ){
				$target = $target.parents(".show-more-toggle");
			}

			if ( $parent.hasClass( "view-more" ) ) {
				$parent.removeClass( "view-more" ); 
			}else{
				$parent.addClass( "view-more" );
			}

			$scope.update_meta_line_depths();

		};

		// Toggle the loading modal window
		$scope.toggle_loading = function( type ){
			var $loading = $("#loading_modal")
			;
			if ( type === "show" && $loading.hasClass("hidden")){
					$loading.removeClass( "hidden" );
			}else{
				if ( !$loading.hasClass( "hidden" ) ){
					$loading.addClass( "hidden" );
				}
			}
		};

		$scope.update_overflow = function(){
			// Check commentary overflows
			$(".commentary .meta-item").each( function( k, elem ){
				if( !is_overflowed_h( elem ) && !$( elem ).hasClass("view-more")){
					$( elem ).children(".show-more-toggle").hide();
				}
			} );

			// Check definitions overflows
			$(".definitions .meta-item").each( function( k, elem ){
				if( !is_overflowed_h( elem ) && !$( elem ).hasClass("view-more") ) {
					$( elem ).children(".show-more-toggle").hide();
				}
			} );
		};

		$scope.check_user_fetch = function(){

		};

    }
  ]);

angular.module('sg')
	.controller('PageController', ['$scope', '$http', function($scope, $http) {

}]);
angular.module('sg')
	.controller('ProfileController', ['$scope', '$http', function($scope, $http) {

	$scope.my_notes = [];

	$scope.fetch_notes = function( ){
	// Get notes and bookmarked lines
		var fetch_notes_params = {};

		$http.post( '/fetch_notes', fetch_notes_params )
					.success(function(data, status, headers, config){
						//$(".bookmarked").removeClass("bookmarked").addClass("not-bookmarked");
						console.log("My Notes Data", data);

						$scope.my_notes = data;

					})
					.error(function(data, status, headers, config){
						console.log("Error:", data);
					});

	};

	$scope.fetch_notes();

}]);
angular.module('sg')
	.controller('SearchController', ['$scope', '$http', function($scope, $http) {


	var Sg = window.__sg__
	,	db = Sg.Db
	,	entity_params = {
					model : 'entities',
					__bind_method__ : "append",
					__resp__ : 'items',
					sort : {'en_name':1},
					offset : 0,
					limit : 21
				}
	,	media_params = {
					model : 'media',
					__bind_method__ : "append",
					__resp__ : 'items',
					sort : {'title':1},
					offset : 0,
					limit : 21
				}
	,	line_params = {
					model : 'lines',
					__bind_method__ : "append",
					sort : {'work':1,'subwork.n':1,'line.n':1},
					offset : 0,
					limit : 21
				}
	,	path = location.pathname.split("/")
	;

	$scope.lines = [];
	$scope.items = [];
	$scope.linked_apps = [];
	$scope.line_offset = 0;
	$scope.item_offset = 0;

	$scope.$watch(
		function () { return document.getElementById('result_lines').innerHTML },
		function( value ){
		// Update to the DOM has occured;
			var val = value || null;

			if ( val ){

				$("#loading_modal").addClass("hidden");
				document.title = "Search | Segetes";

			}

		});

	$scope.init = function(){
		$scope.query_linked();
	};

	$scope.update = function( e ){
		$scope.lines = [];
		$scope.items = [];

		 $("#loading_modal").removeClass("hidden");

		line_params['textsearch'] = $scope.query;
		//entity_params['en_name'] = $scope.query; 
		//media_params['title'] =  $scope.query; 

		// Query this application
		db.query($scope, $http, line_params);

		// Query all the linked applications
		$scope.linked_apps.forEach(function(app){


				$http.post('/api', line_params)
					.success(function(data, status, headers, config){

						console.log("Linked Texts Results:", data);
						app.lines = data.res; 

					})
					.error(function(data, status, headers, config){
						console.log("Error:", data);
					});


			});


		//db.query($scope, $http, entity_params);
		//db.query($scope, $http, media_params);

	};

	$scope.more = function( type, e ) {

		//$(".more").addClass("hidden");
		//$(".loading").removeClass("hidden");

		if ( type === "lines" ){
			$scope.line_offset = $scope.line_offset + 1;
			line_params.offset = $scope.line_offset * line_params.limit;
			db.query($scope, $http, line_params);

		}else if ( type === "items" ){

			//$scope.item_offset = $scope.item_offset + 1;
			//entity_params.offset = $scope.item_offset;
			//media_params.offset = $scope.item_offset;
			//db.query($scope, $http, entity_params);
			//db.query($scope, $http, media_params);

		}


	};


	$scope.query_linked = function(){
	// Query the linked apps

		var params = {
					model : 'linked_apps',
					limit : 0,
					__callback__ : $scope.fetch_linked
				};

		db.query( $scope, $http, params );

	};

	$scope.check_user_fetch = function(){
	// Check the user

	};

	$scope.init();

}]);
angular.module('sg')
	.controller('SingleController', ['$scope', '$http', function($scope, $http) {


	var Sg = window.__sg__
	,	db = Sg.Db
	,	geo = Sg.Geo
	,	path = location.pathname.split("/")
	,	params = {
					include_line_refs : true,
					model : path[1],
					slug : path[2]
				}
	,	lat = 0
	,	lon = 0
	;

	/*
	 * Managing primary update client application lifecycle
	 */
	$scope.update = function( e ){

		db.query($scope, $http, params);

	};

	/* 
	 * Apply response from single query
	 */
	$scope.bind = function() {
		//debugger;

		$scope.items = $scope.entities || $scope.media; 
		if ( $scope.items.length > 0 ){
			$scope.item = $scope.items[0];

			if( typeof $scope.item.geo !== "undefined" ){
				$scope.init_geo();
			}

		}
	};

	/*
	 * If there's geo information for the single item content, load map
	 */
	$scope.init_geo = function(){

		if ( $("#map").length == 0 ){

			setTimeout( function(){geo.init($scope.item.geo.lat, $scope.item.geo.lon, $scope.item.geo.z)}, 500);
		}else{
			geo.init($scope.item.geo.lat, $scope.item.geo.lon, $scope.item.geo.z);
		}

	};

	/*
	 * Init for data-ng-init
	 */
	$scope.init = function(){

		params.__callback__ = $scope.bind;
		$scope.update();

	};




}]);
angular.module('sg')
	.controller('TextController', ['$scope', '$http', function($scope, $http) {

		var Sg 		= window.__sg__ 
		,	db 		= Sg.Db 
		;

		$scope.is_text_page = true;
		$scope.work_title = "";
		$scope.subwork_title = "";
		Sg.with_meta_lines = [];
		// Get user notes if user logged in
		Sg.my_notes = [];

		// Watch the line update and perform necessary post line update fns
		$scope.$watch(
			function () { return document.getElementById('lines').innerHTML },
			function( value ){
			// Update to the DOM has occured;
				var val = value || null;

				if ( val && $(".line").length > 0 ){

					$scope.toggle_loading( 'hide' );
					$scope.update_overflow();

				}

			});

		$scope.init = function(){

			// unhide translation link
			$(".translation-link").removeClass("hidden");

			// Do the first update for the page load
			$scope.update();
		};

		// Update the data based on the path params
		$scope.update = function(e) {

			var	path = location.pathname.split("/")
			,	params = {
					'model' : 'lines',
					'__callback__' : $scope.update_translations
				}
			,	path_lines
			,	i
			;

			path = path.clean("");

			if ( path.length >= 3 ){
				path_lines = path[2].split("-");

				if ( path_lines.length > 1 ){
					params['line.n'] = {
						$gte : parseInt( path_lines[0] ),
						$lte : parseInt( path_lines[1] )
					}
				}else {
					params['line.n'] = {
						$gte : parseInt( path_lines[0] )
					}
					params.limit = 14;
				}

				params['subwork.n'] = parseInt( path[1] );
				params['work'] = path[0]; 


			}else if ( path.length >= 2 ){
				params['work'] = path[0]; 
				params['subwork.n'] = parseInt( path[1] );
				params['line.n'] = {
					$gte : 1
				}
				params.limit = 14;

			}else if (path.length >= 1) {
				params['work'] = path[0]; 
				params['subwork.n'] = 1; 
				params['line.n'] = {
					$gte : 1
				}
				params.limit = 14;

			}else {
				params['work'] = ''; 
				params['subwork.n'] = 1; 
				params['line.n'] = {
					$gte : 1
				}
				params.limit = 14;

			}

			// Show the loading modal window 
			$scope.toggle_loading( 'show' );

			// Do the inital query parsing the path values
			db.query( $scope, $http, params);

			// Set the scope text location values; 
			$scope.set_selected_work( params.work );
			$scope.set_selected_subwork( params['subwork.n'] );
			if ( typeof params['line.n'] === "object" ){
				$scope.line_from = params['line.n']['$gte'];
			}else {
				$scope.line_from = params['line.n'];
			}
			$scope.set_pagination( params );

			$scope.check_user_fetch();

			$scope.scroll_to_top();
		};
		window.onpopstate = function( e ) {
			// Update scope with the line params
			$scope.update();		
		};

		// Show/hide the meta information for the lines
		$scope.toggle_meta = function(e){
			var $target = $(e.target)
			,	$line 	= $target.parents(".line")
			,	$lines  = $(".line")
			;

			// Toggle the meta information based on all the lines
			if($line.hasClass("with-meta")){

				$line.removeClass("with-meta");
				$scope.update_meta_line_depths();
				Sg.with_meta_lines = Sg.with_meta_lines
					.filter(function (line) {
						return line.n !== $line.data().n;
					});
				if ( $line.data().n === Sg.lemma_fixed_n ){
					delete Sg.lemma_fixed_n;
				}

				if(!($lines.hasClass("with-meta"))){
					$("header").removeClass("header-hidden");
					$lines.removeClass("extended");
				}

			} else {

				$line.addClass("with-meta");
				$scope.update_meta_line_depths();
				Sg.with_meta_lines.push({
						'n' : $line.data().n,
						'h' : $line.offset().top
					});
				Sg.with_meta_lines.sort(function(a, b){
						return a.h - b.h;
					});


				if(!($lines.hasClass("extended"))){
					$("header").addClass("header-hidden");
					$lines.addClass("extended");
				}
			}

		};

		// Update the selected line depths each time height is changed 
		$scope.update_meta_line_depths = function(){
			Sg.with_meta_lines.forEach(function(e){
					e.h = $(".line[data-n='" + e.n + "']").offset().top;
				});
		};

		// Show/hide the selected meta section
		// Sections: definitions, comments, articles, related passages
		$scope.toggle_meta_section = function(e){
			var $target  = $(e.target)
			,	$line 	 = $target.parents(".line")
			,	$section = $line.children(".meta").children(".meta-section." + $target.data().type)
			;

			$target.toggleClass("selected");
			$section.toggleClass("meta-selected");
			$scope.update_meta_line_depths();


		};

		// Show/hide the scansion highlighting
		$scope.toggle_scansion = function(e){
			var $target = $(e.target)
			,	$line 	= $target.parents(".line")
			;
			$target.toggleClass("selected");
			$line.toggleClass("show-scansion");

		};

		$scope.update_overflow = function(){
			// Check commentary overflows
			$(".commentary .meta-item").each( function( k, elem ){
				if( !is_overflowed_h( elem ) && !$( elem ).hasClass("view-more")){
					$( elem ).children(".show-more-toggle").hide();
				}
			} );

			// Check definitions overflows
			$(".definitions .meta-item").each( function( k, elem ){
				if( !is_overflowed_h( elem ) && !$( elem ).hasClass("view-more") ) {
					$( elem ).children(".show-more-toggle").hide();
				}
			} );

			// Check notes overflows
			$(".my_notes .meta-item").each( function( k, elem ){
				if( !is_overflowed_h( elem ) && !$( elem ).hasClass("view-more") ) {
					$( elem ).children(".show-more-toggle").hide();
				}
			} );
		};

		$scope.show_more = function( e ){

			var $target = $(e.target)
			,	$parent = $target.parents(".meta-item")
			;

			if ( !$target.hasClass("show-more-toggle") ){
				$target = $target.parents(".show-more-toggle");
			}

			if ( $parent.hasClass( "view-more" ) ) {
				$parent.removeClass( "view-more" ); 
			}else{
				$parent.addClass( "view-more" );
			}

			$scope.update_meta_line_depths();

		};

		// Toggle the loading modal window
		$scope.toggle_loading = function( type ){
			var $loading = $("#loading_modal")
			;
			if ( type === "show" && $loading.hasClass("hidden")){
					$loading.removeClass( "hidden" );
			}else{
				if ( !$loading.hasClass( "hidden" ) ){
					$loading.addClass( "hidden" );
				}
			}
		};

		// Toggle the browse tool panels
		$scope.toggle_browse_panel = function( e ) {
			var $target = $( e.target )
			,	$parent = $target.parents(".tool-browser")	
			,	$panel = $parent.children(".tool-panel")
			;

			if ( $panel.hasClass("hidden") ){
				$panel.removeClass("hidden");
			}else{
				$panel.addClass("hidden");
			}

		};

		// Browse the texts via AJAX
		$scope.browse = function( e ){
			var $target = $()
			,	new_path = '/'
			;	

			// Normalize event
			if ( e ){
				$target = $( e.target )
			}

			// Unfix all lines
			$(".lemma-fixed").removeClass("lemma-fixed");
			$(".header-hidden").removeClass("header-hidden");
			delete Sg.lemma_fixed_n;
			delete Sg.old_lemma_fixed_n;
			delete Sg.lemma_fixed_h;
			delete Sg.lemma_fixed_n_to_unfix;
			Sg.with_meta_lines = [];

			// Set the options based on the selected target 
			if ( $target.hasClass("work-option") ){
				$scope.set_selected_work( $target.data().slug );
				$scope.set_selected_subwork( 1 );
				$scope.line_from = 1;

			}else if ( $target.hasClass("subwork-option") ){
				$scope.set_selected_subwork( parseInt( $target.data().n ) );
				$scope.line_from = 1;

			}else {
				// else, it's a line update
				if ( $scope.line_from.length > 0 ) {
					$scope.line_from = $scope.check_line_input( $scope.line_from );
				}
			}


			// Finally, do a query with the new parameters
			db.query( $scope, $http, {
					'model' : "lines",
					'work' : $scope.work,
					'subwork.n' : $scope.subwork,
					'line.n': {
							$gte : parseInt( $scope.line_from ) 
						},
					'__callback__' : $scope.update_translations,
					'limit' : 14
				});
			$scope.set_pagination({
					'work' : $scope.work,
					'subwork.n' : $scope.subwork,
					'line.n' : $scope.line_from,
					'limit' : 14
				});

			new_path = new_path + $scope.work + "/" + $scope.subwork + "/" + $scope.line_from;
			history.pushState({
					'work' : $scope.work,
					'subwork.n' : $scope.subwork,
					'line.n': {
							$gte : parseInt( $scope.line_from ) 
						},
				}, "", new_path);

			// Finally, raise all panels
			$(".tool-panel").addClass("hidden");

			// Show the loading window to be unhidden on the line DOM manipulation
			$scope.toggle_loading( 'show' );

			$scope.scroll_to_top();
		};

		// Convert to Roman Numeral
		$scope.to_roman_numeral = function( n ){
			var l = "";
			if ( n === 1 ) {
				l = "I";
			}else if ( n === 2 ){
				l = "II";
			}else if ( n === 3 ){
				l = "III";
			}else if ( n === 4 ){
				l = "IV";
			}else if ( n === 5 ){
				l = "V";
			}else if ( n === 6 ){
				l = "VI";
			}else if ( n === 7 ){
				l = "VII";
			}else if ( n === 8 ){
				l = "VIII";
			}else if ( n === 9 ){
				l = "IX";
			}else if ( n === 10 ){
				l = "X";
			}else if ( n === 11 ){
				l = "XI";
			}else if ( n === 12 ){
				l = "XII";
			}
			return l;
		};

		// Set the selected work from the works
		$scope.set_selected_work = function( work ){
			if ( work == "aeneid" ) {
				$scope.selected_work = $scope.works[0];
			}else if ( work === "georgics" ){
				$scope.selected_work = $scope.works[1];
			}else if ( work === "eclogues" ){
				$scope.selected_work = $scope.works[2];
			}else {
				$scope.selected_work = null;
			} 

			$scope.work = work;
		};

		// Set the selected subwork via the roman numeral
		$scope.set_selected_subwork = function( subwork ){
			$scope.selected_subwork = {
											'n' : subwork,
											'title' : $scope.to_roman_numeral( subwork )
										};			
			$scope.subwork = subwork;
		};
		$scope.works = [
				{
					'title' : 'Aeneid',
					'guid' : '/aeneid/',
					'slug' : 'aeneid',	
					'meta_elements' : {
						'online' : 0,
						'entities' : 1023,
						'media' : 213,
						'related_passages' : 23875,
						'articles' : 5098 
					},
					'subworks' : [
						{
							'title' : 'I',
							'n' : 1, 
							'guid' : '/aeneid/1/' 
						},
						{
							'title' : 'II',
							'n' : 2, 
							'guid' : '/aeneid/2/' 
						},
						{
							'title' : 'III',
							'n' : 3, 
							'guid' : '/aeneid/3/' 
						},
						{
							'title' : 'IV',
							'n' : 4, 
							'guid' : '/aeneid/4/' 
						},
						{
							'title' : 'V',
							'n' : 5, 
							'guid' : '/aeneid/5/' 
						},
						{
							'title' : 'VI',
							'n' : 6, 
							'guid' : '/aeneid/6/' 
						},
						{
							'title' : 'VII',
							'n' : 7, 
							'guid' : '/aeneid/7/' 
						},
						{
							'title' : 'VIII',
							'n' : 8, 
							'guid' : '/aeneid/8/' 
						},
						{
							'title' : 'IX',
							'n' : 9, 
							'guid' : '/aeneid/9/' 
						},
						{
							'title' : 'X',
							'n' : 10, 
							'guid' : '/aeneid/10/' 
						},
						{
							'title' : 'XI',
							'n' : 11, 
							'guid' : '/aeneid/10/' 
						},
						{
							'title' : 'XII',
							'n' : 12, 
							'guid' : '/aeneid/12/' 
						}
					]
				},
				{
					'title' : 'Georgics',
					'guid' : '/georgics/',
					'slug' : 'georgics',
					'meta_elements' : {
						'online' : 0,
						'entities' : 83,
						'media' : 31,
						'related_passages' : 9018,
						'articles' : 341 
					},
					'subworks' : [
						{
							'title' : 'I',
							'n' : 1, 
							'guid' : '/georgics/1/' 
						},
						{
							'title' : 'II',
							'n' : 2, 
							'guid' : '/georgics/2/' 
						},
						{
							'title' : 'III',
							'n' : 3, 
							'guid' : '/georgics/3/' 
						},
						{
							'title' : 'IV',
							'n' : 4, 
							'guid' : '/georgics/4/' 
						}
					]
				},
				{

					'title' : 'Eclogues',
					'guid' : '/eclogues/',
					'slug' : 'eclogues',
					'meta_elements' : {
						'online' : 0,
						'entities' : 163,
						'media' : 28,
						'related_passages' : 658,
						'articles' : 234 
					},
					'subworks' : [
						{
							'title' : 'I',
							'n' : 1, 
							'guid' : '/eclogues/1/' 
						},
						{
							'title' : 'II',
							'n' : 2, 
							'guid' : '/eclogues/2/' 
						},
						{
							'title' : 'III',
							'n' : 3, 
							'guid' : '/eclogues/3/' 
						},
						{
							'title' : 'IV',
							'n' : 4, 
							'guid' : '/eclogues/4/' 
						},
						{
							'title' : 'V',
							'n' : 5, 
							'guid' : '/eclogues/5/' 
						},
						{
							'title' : 'VI',
							'n' : 6, 
							'guid' : '/eclogues/6/' 
						},
						{
							'title' : 'VII',
							'n' : 7, 
							'guid' : '/eclogues/7/' 
						},
						{
							'title' : 'VIII',
							'n' : 8, 
							'guid' : '/eclogues/8/' 
						},
						{
							'title' : 'IX',
							'n' : 9, 
							'guid' : '/eclogues/9/' 
						},
						{
							'title' : 'X',
							'n' : 10, 
							'guid' : '/eclogues/10/' 
						}
					]
				}
			];
		$scope.set_selected_work( $scope.work );
		$scope.set_selected_subwork( $scope.subwork );

		// Check line input for only ints, only four digits
		$scope.check_line_input = function( val ) {

			if ( !( /^\d+$/.test(val) ) ){ 
				val = val.replace(/\D/g,'');
			}

			if ( val.length > 4 ){
				val = val.substring(0,4);
			}

			return val;
		};

		$scope.set_pagination = function( params ){
			var lengths = {
					'aeneid' : [756,804,718,705,871,901,817,731,818,908,915,952],
					'georgics' : [514,542,566,566],
					'eclogues' : [84,73,111,63,90,86,70,109,67,77]
				}
			,	work_length = lengths[ params.work ][ params['subwork.n'] - 1 ] 
			;


			// Prev pagination
			$scope.pag = $scope.pag || { prev : { to : 0, from : 0, link : "/" }, next : { to : 0, from : 0, link : "/" } };

			// Ensure line_from is integer
			$scope.line_from = parseInt( $scope.line_from );
			if ( $scope.line_from < work_length ){

				if ( $scope.line_from > 0 && $scope.line_from < params.limit && $scope.line_from !== 1 ){
					$scope.pag.prev.to = params.limit; 
				}else if ( $scope.line_from > 0 ){
					$scope.pag.prev.to = $scope.line_from;
				}else {
					$scope.pag.prev.to = 1;
				}
				if ( $scope.line_from - (params.limit + 1) > 0 ){
					$scope.pag.prev.from = $scope.line_from - (params.limit + 1);
				}else {
					$scope.pag.prev.from = 1;
				}
				$scope.pag.prev.link =  "/" + params.work + "/" + params['subwork.n'] + "/" + $scope.pag.prev.from;

				// Next Pagination
				if ( $scope.line_from + params.limit < work_length ){
					$scope.pag.next.from = $scope.line_from + params.limit;
				}else {
					$scope.pag.next.from = work_length;
				}
				if ( $scope.line_from + (params.limit + params.limit) < work_length ){
					$scope.pag.next.to = $scope.line_from + (params.limit + params.limit);
				}else {
					$scope.pag.next.to = work_length;
				}
				$scope.pag.next.link =  "/" + params.work + "/" + params['subwork.n'] + "/" + $scope.pag.next.from;
			}else {
				$scope.pag.prev.from = 0;
				$scope.pag.prev.to = 0;
				$scope.pag.next.from = 0;
				$scope.pag.next.to = 0;
			}


		};

		$scope.upload_media = function( e ){

			if( $scope.check_upload_form( e ) ){

				$http.post('/send_modal', $scope.upload)
					.success(function(data, status, headers, config){
						$scope.upload_modal_toggle( {} );
						$scope.success_modal_toggle();
					});
			}

		};

		$scope.check_upload_form = function( e ){


			return true;
		};

		$scope.success_modal_toggle = function(){
			$("#success_modal").toggleClass("hidden");
		};

		$scope.upload_modal_toggle = function( e ){
			var $target 
			,	$line
			;

			if ( typeof e.target !== "undefined" ) {
				$target = $(e.target);

				if ( !$target.hasClass("modal-close") ){
					$line = $target.parents( ".line" );
					$scope.upload = {
						'line_n' : $line.data().n,
						'work' : $.trim( $( "h1.work_title" ).text().toLowerCase() ),
						'subwork' : $.trim( $( "h2.subwork_title" ).text() )
					};
				}
			}

			$("#upload_modal").toggleClass("hidden");
		};

		$scope.modal_hide = function( e ){
		// Hide whatever modal
			$(".text-modal").addClass("hidden");

		};

		$scope.fetch_notes = function( ){
		// Get notes and bookmarked lines
			var fetch_notes_params = {
					'ref.work' : $scope.work,
					'ref.subwork.n' : $scope.subwork,
					'ref.line.n' : {
								'$gte' : $scope.line_from,
								'$lte' : $scope.line_from + 14 
							}
				};

			$http.post( '/fetch_notes', fetch_notes_params )
						.success(function(data, status, headers, config){
							//$(".bookmarked").removeClass("bookmarked").addClass("not-bookmarked");
							console.log("My Notes Data", data);

							$scope.lines.forEach(function(line){

								line.my_notes = [];
								data.forEach(function(note){
									if ( parseInt( note.ref.line.n ) === line.line.n){
										$(".line[data-n='" + note.ref.line.n + "'] .not-bookmarked").addClass("bookmarked").removeClass("not-bookmarked");
										line.my_notes.push( note );
									}
								});

							});

							$scope.update_overflow();
						})
						.error(function(data, status, headers, config){
							console.log("Error:", data);
						});

		};

		$scope.add_note = function( e ){
		// Process Note params, toggle bookmarked, and post to /save_note
			var $target = $( e.target )
			;

			if ( !$target.hasClass("meta-tab") ){
				$target = $target.parents(".meta-tab");
			}
			$scope.note = {
							ref : $scope.get_note_ref( $target ),
							text:""
						};

			if ( $target.hasClass("not-bookmarked") ){
				$("#save_note_modal").removeClass("hidden");
			}

			if ( $target.hasClass("bookmarked") ){
			// If it already has a note on the line, get the note on the line
				$scope.get_update_note( e );

			}

		};

		$scope.get_update_note = function( e ){

			var $target = $( e.target )
			,	fetch_notes_params = {
					'ref.work' : $scope.work,
					'ref.subwork.n' : $scope.subwork,
					'ref.line.n' : $target.parents(".line").data().n
				}
			;

			$http.post( '/fetch_notes', fetch_notes_params )
						.success(function(data, status, headers, config){
							//$(".bookmarked").removeClass("bookmarked").addClass("not-bookmarked");
							console.log("My Notes Update Data", data);

							data.forEach(function(note){
								$scope.note = note;
							});
							$("#update_note_modal").removeClass("hidden");

						})
						.error(function(data, status, headers, config){
							console.log("Error:", data);
						});
		};

		$scope.get_note_ref = function( $target ){
		// Get the note params from the target selected on the line
			var ref = {
					work : '',
					subwork : {
							n : 0
						},
					line : {
							n : 0
						}
				}
			;

			ref.work = $scope.work;
			ref.subwork.n = $scope.subwork;
			ref.line.n = $target.parents(".line").data().n

			return ref;
		};

		$scope.save_note = function( ){
		// save the note
			$http.post('/save_note', $scope.note)
						.success(function(data, status, headers, config){
							console.log("Note saved!");
							$("#save_note_modal").addClass("hidden");
							$scope.fetch_notes();

						})
						.error(function(data, status, headers, config){
							alert("We're sorry, it looks there was a problem saving your note. Please let us know what happened at contact@segetes.io, and we'll take a look at it.")
							console.log("Error:", data);
						});

		};

		$scope.update_note = function( ){
		// update the note on the line
			$http.post('/update_note', $scope.note)
						.success(function(data, status, headers, config){
							console.log("Note updated!");
							$("#update_note_modal").addClass("hidden");
							$scope.fetch_notes();
						})
						.error(function(data, status, headers, config){
							alert("We're sorry, it looks there was a problem saving your note. Please let us know what happened at contact@segetes.io, and we'll take a look at it.")
							console.log("Error:", data);
						});

		};

		$scope.remove_note = function( e ){
		// post to remove note to remove a note
			var $target = $( e.target )
			;

			$scope.note = {
							ref : {subwork:{},line:{}},
							text:""
						};

			$scope.note.ref.work = $scope.work;
			$scope.note.ref.subwork.n = $scope.subwork;
			$scope.note.ref.line.n = $target.parents(".line").data().n;



			$http.post('/remove_note', $scope.note)
						.success(function(data, status, headers, config){
							console.log("Note removed:", data);
							$(".line[data-n='" + $scope.note.ref.line.n + "'] .bookmarked").addClass("not-bookmarked").removeClass("bookmarked");
							$scope.fetch_notes();
						})
						.error(function(data, status, headers, config){
							alert("We're sorry, it looks there was a problem removing your note. Please let us know what happened at contact@segetes.io, and we'll take a look at it.")
							console.log("Error:", data);
						});

		};

		$scope.check_user_fetch = function() {
		// Check if a user is logged in
			$http.post('/auth/check', $scope.note)
				.success(function(data, status, headers, config){
					if (data === true){
						$scope.fetch_notes();
					}
				})
				.error(function(data, status, headers, config){
					console.log("Error:", data);
				});

		};

		$scope.toggle_translation = function() {
			// toggle translation 

			$("#loading_modal").addClass("hidden");
			$("#translation_panel").fadeIn(200);
			$(".translation-tool").addClass(".translation-active");

		};

		$scope.get_translations = function(){
			var translation_params = {
					model : "translations",
					work : $scope.work,
					'subwork.n' : $scope.subwork,
					'edition.slug' : $scope.selected_translation_edition,
					'line.n' : {
								'$gte' : $scope.selected_trans_range.min,
								'$lte' : $scope.selected_trans_range.max 
							}//,
					//'__callback__' : $scope.update_translations
				};

			//$("#loading_modal").removeClass("hidden");
			db.query($scope, $http, translation_params);

		};

		$scope.update_translations = function(){
		// Gather some metadata from the lines about the avaiable translations
			var available_editions = []
			;

			// First get all the available translation editions and thier max/min
			$scope.lines.forEach(function(line){

					line.translations.forEach(function(trans){

						var is_in_av_ed = false;

						available_editions.forEach(function(ed){
								if ( ed.slug === trans.edition.slug ) {
									is_in_av_ed = true;
									ed.line_ns.push.apply( ed.line_ns, trans.l_ns );
								}

							});

						if ( !is_in_av_ed ) {
							available_editions.push({
									slug : trans.edition.slug, 
									line_ns : trans.l_ns
								});
						}


					});

				});

			// Then set the selected translation
			$scope.available_editions = available_editions;
			$scope.set_selected_translation();

		};

		$scope.set_selected_translation = function( e ){
		// Set the selected translation for the translation pane
			var min = 0
			,	max = 0
			, 	selected_translation = {}
			,	sel_ed = ''
			,	$target
			;

			if ( e && typeof e.target !== "undefined" ) {
				// Update the selected translation based on the event
				$target = $(e.target);

				if ( !$target.hasClass("option") ){
					$target = $target.parents(".option");
				}

				sel_ed = $target.data().edition;
				$scope.available_editions.forEach(function(ed){
						if ( ed.slug === sel_ed ){
							selected_translation = ed;
						}
					});

			}else {
				// Otherwise, set the default selected translation
				if ( $scope.available_editions.length > 0 ){
					selected_translation = $scope.available_editions[0];
				}

			}

			$scope.selected_translation_edition = selected_translation.slug; 

			min = Math.min.apply(null, selected_translation.line_ns),
			max = Math.max.apply(null, selected_translation.line_ns);


			$scope.selected_trans_range = {
					min : min,
					max : max
				};

			$scope.get_translations();

		};

		$scope.close_translation = function(){
			$("#translation_panel").fadeOut(200);
			$(".translation-active").removeClass(".translation-active");

		};

		$scope.scroll_to_top = function() {
		// scroll to the top of the page
			$('html, body').animate({scrollTop : 0},200);
		};


		$scope.init();

    }
  ]);

Array.prototype.clean = function(deleteValue) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] == deleteValue) {         
      this.splice(i, 1);
      i--;
    }
  }
  return this;
};

function is_overflowed_h(element){
    return element.scrollHeight > element.clientHeight + 10;
}

angular.module('sg')
	.controller('TextIndexController', ['$scope', '$http', function($scope, $http) {

		var Sg = window.__sg__
		,	db = Sg.Db
		,	author_params = {type:"authors"}
		;

		$scope.authors = [{
			'display_name' : 'Vergil',
			'guid' : '/author/vergil',
			'texts' : [
				{
					'title' : 'Aeneid',
					'guid' : '/aeneid/',
					'meta_elements' : {
						'lines' : 9896,
						'entities' : 170,
						'media' : 0,
						'related_passages' : 6066,
						'articles' : 31730,
						'commentary' : 0 
					},
					'subworks' : [
						{
							'title' : 'I',
							'guid' : '/aeneid/1/' 
						},
						{
							'title' : 'II',
							'guid' : '/aeneid/2/' 
						},
						{
							'title' : 'III',
							'guid' : '/aeneid/3/' 
						},
						{
							'title' : 'IV',
							'guid' : '/aeneid/4/' 
						},
						{
							'title' : 'V',
							'guid' : '/aeneid/5/' 
						},
						{
							'title' : 'VI',
							'guid' : '/aeneid/6/' 
						},
						{
							'title' : 'VII',
							'guid' : '/aeneid/7/' 
						},
						{
							'title' : 'VIII',
							'guid' : '/aeneid/8/' 
						},
						{
							'title' : 'IX',
							'guid' : '/aeneid/9/' 
						},
						{
							'title' : 'X',
							'guid' : '/aeneid/10/' 
						},
						{
							'title' : 'XI',
							'guid' : '/aeneid/10/' 
						},
						{
							'title' : 'XII',
							'guid' : '/aeneid/12/' 
						}
					]
				},
				{
					'title' : 'Georgics',
					'guid' : '/georgics/',
					'meta_elements' : {
						'lines' : 2188,
						'entities' : 83,
						'media' : 0,
						'related_passages' : 994,
						'articles' : 7219,
						'commentary' : 0 
					},
					'subworks' : [
						{
							'title' : 'I',
							'guid' : '/georgics/1/' 
						},
						{
							'title' : 'II',
							'guid' : '/georgics/2/' 
						},
						{
							'title' : 'III',
							'guid' : '/georgics/3/' 
						},
						{
							'title' : 'IV',
							'guid' : '/georgics/4/' 
						}
					]
				},
				{

					'title' : 'Eclogues',
					'guid' : '/eclogues/',
					'meta_elements' : {
						'lines' : 830,
						'entities' : 53,
						'media' : 0,
						'related_passages' : 349,
						'articles' : 5366,
						'commentary' : 0 
					},
					'subworks' : [
						{
							'title' : 'I',
							'guid' : '/eclogues/1/' 
						},
						{
							'title' : 'II',
							'guid' : '/eclogues/2/' 
						},
						{
							'title' : 'III',
							'guid' : '/eclogues/3/' 
						},
						{
							'title' : 'IV',
							'guid' : '/eclogues/4/' 
						},
						{
							'title' : 'V',
							'guid' : '/eclogues/5/' 
						},
						{
							'title' : 'VI',
							'guid' : '/eclogues/6/' 
						},
						{
							'title' : 'VII',
							'guid' : '/eclogues/7/' 
						},
						{
							'title' : 'VIII',
							'guid' : '/eclogues/8/' 
						},
						{
							'title' : 'IX',
							'guid' : '/eclogues/9/' 
						},
						{
							'title' : 'X',
							'guid' : '/eclogues/10/' 
						}
					]
				}
			]
		}];

		$scope.init = function(){
			$scope.query_linked();
		};


		$scope.query_linked = function(){

			var params = {
						model : 'linked_apps',
						limit : 0,
						__callback__ : $scope.fetch_linked
					};

			db.query( $scope, $http, params );

		};

		$scope.fetch_linked = function(){


			$scope.linked_apps.forEach(function(app){

					app.authors = [];

					$http.post( app.url + "/api", { manifest : true } )
						.success(function(data, status, headers, config){
							console.log("Linked Text Response:", data);

							app.authors = data.res;

						})
						.error(function(data, status, headers, config){
							console.log('Error', data);
						});

				});



		};

		$scope.init();
    }
  ]);

/*
 *
 * db.js - custom functions for querying the application api 
 *
 */

(function($){

window.__sg__ = window.__sg__ || {};
var Sg = window.__sg__;

Sg.Db = {

	/* 
     * Save post data to database (currently not used)
     */
		save : function($scope, $http, params){

						console.log("Save:", params);

						$http.post('/api/s', params)
							.success(function(data, status, headers, config){
								//console.log(data);
							})
							.error(function(data, status, headers, config){
								//console.log('error', data);
							});

					}
	/* 
     * Query data from database and set custom callbacks, scope assignment, etc. 
     */
	,	query : function($scope, $http, params){

						var callback
						;

						console.log("Query:", params);

						// set callback from the params
						if ( typeof params.__callback__ !== "undefined" ){
							callback = params.__callback__;
							delete params.__callback__;
						}

						// If the Segetes object is set to is_mobile, enforce query limit of 5
						if ( typeof Sg.is_mobile !== "undefined" ){
							if ( Sg.is_mobile === true && params.model === "lines" ){
								params.limit = 5;
							}

						// Otherwise, if the browser window is small enough to be mobile 
						} else if ( window.innerWidth < 600 && params.model === "lines" ){
								params.limit = 5;
						}

						// Do post to API with params
						$http.post('/api', params)
							.success(function(data, status, headers, config){

								// Set the key on scope that the response should be set
								if ( typeof data.__resp__ !== "undefined" ){
									data.model = data.__resp__;
								}

								// Set the no more items flag
								if ( typeof data.limit !== "undefined" ){
									if ( data.res.length <= data.limit - 1 ){
										$scope.no_more_items = true;
									}
								}else {
									if ( data.res.length <= 0 ){
										$scope.no_more_items = true;
									}
								}
								// Hide modal window just in case there's no items
								if ( data.res.length === 0 ){
									$("#loading_modal").addClass("hidden");
								}

								// If it is with lines, parse the line text as JSON
								if ( typeof data.include_line_refs !== "undefined" ){
									data.res.forEach(function(e){
										if ( typeof e.lines !== "undefined" ){
											e.lines = JSON.parse( e.lines );

										}
									});
								}

								console.log("Response:", data);

								// Check the phase to apply the data based on the posted bind_method
								if(!$scope.$$phase){
									$scope.$apply(function(){

										if ( data.__bind_method__ === "append" ){
											data.res.forEach(function(e){
												$scope[ data.model ].push( e );
											});

										}else {

											$scope[data.model] = data.res;
										}

									});
								}else {

									if ( data.__bind_method__ === "append" ){
										data.res.forEach(function(e){
											$scope[ data.model ].push( e );
										});

									}else {
										$scope[data.model] = data.res;
									}

								}

								// If it's a lines query, update the titles to reflect the lines
								if ( data.model == "lines" && data.res.length > 0 ){

									$scope.work_title = data.res[0].work.charAt(0).toUpperCase() + data.res[0].work.slice(1);
									$scope.subwork_title = data.res[0].subwork.title.charAt(0).toUpperCase() + data.res[0].subwork.title.slice(1);
									document.title = $scope.work_title + " " + $scope.subwork_title + " | Segetes";
									
									// If it's a lines query and the user is logged in, also fetch the nodes
									$scope.check_user_fetch();

								}

								// If there's a callback, then do the callback
								if ( typeof callback !== "undefined" ){
									callback();
								}

							})

							// Otherwise, handle the errors from the server 
							.error(function(error, status, headers, config){

								console.log("Error:", error);

							});

			}
	};

})( jQuery );

/*
 *
 * geo.js - for handling mapping issues 
 *
 */

(function($){

window.__sg__ = window.__sg__ || {};
var Sg = window.__sg__;

Sg.Geo = {
		init : function(lat, lon, z, set_marker){
			z = typeof z !== "undefined" ? z : 7;
			set_marker = typeof set_marker !== "undefined" ? set_marker : true;

			var center = new google.maps.LatLng( lat, lon );

			// Create an array of styles.
			var styles = [
				{
					stylers: [
						{ hue: "#d9d6bd" },
						{ saturation: 0 }
					]
				},{
					featureType: "road",
					elementType: "geometry",
					stylers: [
						{ hue: "#482f00" },
						{ visibility: "simplified" }
					]
				},{
					featureType: "highway",
					elementType: "geometry",
					stylers: [
						{ hue: "#482f00" },
						{ saturation: -50 },
						{ visibility: "simplified" }
					]
				},{
					featureType: "road",
					elementType: "labels",
					stylers: [
						{ visibility: "off" }
					]
				},{
				    featureType: "poi.business",
				    elementType: "labels",
				    stylers: [
				      { visibility: "off" }
				    ]
				},{
				    featureType: "water",
				    elementType: "geometry",
				    stylers: [
						{ lightness: 100 },
						{ visibility: "simplified" }
				    ]
				},{
				    featureType: "transit",
				    elementType: "labels",
				    stylers: [
				      { visibility: "off" }
				    ]
				},{
				    featureType: "transit",
				    elementType: "geo",
				    stylers: [
				      { visibility: "off" }
				    ]
				  }
			];

			// Create a new StyledMapType object, passing it the array of styles,
			// as well as the name to be displayed on the map type control.
			var styledMap = new google.maps.StyledMapType(styles,
										{name: "Styled Map"});

			// Create a map object, and include the MapTypeId to add
			// to the map type control.
			var mapOptions = {
					zoom: z,
					center: center,
					disableDefaultUI: true,
					mapTypeControlOptions: {
						mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
					}
				};
			var map = new google.maps.Map(document.getElementById('map'),
				mapOptions);

			//Associate the styled map with the MapTypeId and set it to display.
			map.mapTypes.set('map_style', styledMap);
			map.setMapTypeId('map_style');
			// Set the map tilt
			map.setTilt(45);


			if ( set_marker ){
				var latLong = new google.maps.LatLng(lat, lon);
				var marker = new google.maps.Marker({
				  position: latLong,
				  map: map//,
				  //title: name,
				  //post_id : e.post_id,
				  //icon: icon_image
				});
			}



			return map;

		}
	};

})( jQuery );

/*
 *
 * util.js - segetes custom util helps 
 *
 */

(function($){

window.__sg__ = window.__sg__ || {};
var Sg = window.__sg__;

Sg.Util = {

	toggle_nav_fix : function($target, fix_type) {

		if (fix_type === "fix") {
			if (!($target.hasClass("fixed"))){
				$target.addClass("fixed");
			}
		}else{
			if ($target.hasClass("fixed")){
				$target.removeClass("fixed");
			}
		}
	},

	slugify : function( string ) {
		return string 
		    .toLowerCase()
		    .replace(/[^\w ]+/g,'')
		    .replace(/ +/g,'-')
		    ;
	}


}

})( jQuery );

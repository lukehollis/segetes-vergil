/*
 * Initialize the angular application 
 * with primary module aw
 */

angular.module('sg', ['sgFilters', 'ngSanitize', 'headroom']);


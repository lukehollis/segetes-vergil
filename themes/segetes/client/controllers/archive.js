angular.module('sg')
	.controller('ArchiveController', ['$scope', '$http', function($scope, $http) {

	var Sg = window.__sg__
	,	db = Sg.Db
	,	model = $(".ng-archive").data().model
	,	params = {
			model : model, 
			__resp__ : 'items',
			sort : { 'en_name' : 1, 'title' : 1 },
			limit : 21
		}
	,	path = location.pathname.split("/")
	;

	$scope.items = [];
	$scope.model = model;
	Sg.pg = 0;


	$scope.$watch(
		function () { return document.getElementById('items').innerHTML },
		function( value ){
			var val = value || null;

			if ( val ){

				$(".more").removeClass("hidden");
				$(".loading").addClass("hidden");
				$("#loading_modal").fadeOut("fast");

			}

		});

	$scope.$watch(
		function () { return document.getElementById('subnav').innerHTML },
		function( value ){
			var val = value || null;

			if ( val ){

				if ( location.hash.length > 0  && $(".selected").length === 0 ){
					
					$(".subnav-link[data-n='" + location.hash.replace("#", "") + "'").addClass("selected");
				}

			}
		});

	
	if ( model === "something_that_needs_an_alphabet_subnav" ){
		$scope.subnav = [	{
								'title' : "a",
								'slug' : "a"
							},
							{
								'title' : "b",
								'slug' : "b"
							},
							{
								'title' : "c",
								'slug' : "c"
							},
							{
								'title' : "d",
								'slug' : "d"
							},
							{
								'title' : "e",
								'slug' : "e"
							},
							{
								'title' : "f",
								'slug' : "f"
							},
							{
								'title' : "g",
								'slug' : "g"
							},
							{
								'title' : "h",
								'slug' : "h"
							},
							{
								'title' : "i",
								'slug' : "i"
							},
							{
								'title' : "j",
								'slug' : "j"
							},
							{
								'title' : "k",
								'slug' : "k"
							},
							{
								'title' : "l",
								'slug' : "l"
							},
							{
								'title' : "m",
								'slug' : "m"
							},
							{
								'title' : "n",
								'slug' : "n"
							},
							{
								'title' : "o",
								'slug' : "o"
							},
							{
								'title' : "p",
								'slug' : "p"
							},
							{
								'title' : "q",
								'slug' : "q"
							},
							{
								'title' : "r",
								'slug' : "r"
							},
							{
								'title' : "s",
								'slug' : "s"
							},
							{
								'title' : "t",
								'slug' : "t"
							},
							{
								'title' : "u",
								'slug' : "u"
							},
							{
								'title' : "v",
								'slug' : "v"
							},
							{
								'title' : "w",
								'slug' : "w"
							},
							{
								'title' : "x",
								'slug' : "x"
							},
							{
								'title' : "y",
								'slug' : "y"
							},
							{
								'title' : "z",
								'slug' : "z"
							}
						];
	}else {
		$scope.subnav = [	{
								'title' : "Eclogues",
								'slug' : "eclogues"
							},
							{
								'title' : "Georgics",
								'slug' : "georgics"
							},
							{
								'title' : "Aeneid",
								'slug' : "aeneid"
							}
						];
	}


	$scope.more = function( e ){
		$(".more").addClass("hidden");
		$(".loading").removeClass("hidden");

		Sg.pg = Sg.pg + 1;
		params.__bind_method__ = "append";
		$scope.update();

	};

	$scope.filter = function( e ){
		var $target = [] 
		,	f		= '' 
		;

		$("#loading_modal").fadeIn("fast");

		if ( typeof e !== "undefined" ){
			$target = $( e.target );
			f = $target.data().n
			if ( $target.hasClass("selected") ){
				console.log("foo");
				f = ''; 
				location.hash = ''; 
				$target.removeClass("selected");
				delete params.works;
			}else {
				$(".selected").removeClass("selected");
				$target.addClass("selected");
				params.works = f;
				location.hash = f;
			}
		}else {
			f = location.hash.replace("#", "")
			//$(".subnav-link[data-n='" + f + "'").addClass("selected");
			params.works = f;
		}


		Sg.pg = 0;
		params.__bind_method__ = "";

		$scope.update();

	};

	$scope.update = function( e ){

		params.offset = Sg.pg * params.limit; 
		db.query($scope, $http, params);

	};

	if ( location.hash.length > 0 ){
		$scope.filter();
	}else {
		$scope.update();
	}

}]);
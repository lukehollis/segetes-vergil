angular.module('sg')
	.controller('IndexController', ['$scope', '$http', function($scope, $http) {

		var Sg = window.__sg__
		,	db = Sg.Db
		,	params = {
					'model' : 'lines',
					'work' : 'georgics',
					'subwork.n' : 1,
					'line.n' : 1
			}
		;

		$scope.authors = [{
			'display_name' : 'Vergil',
			'guid' : '/author/vergil',
			'texts' : [
				{
					'title' : 'Aeneid',
					'guid' : '/aeneid/',
					'meta_elements' : {
						'lines' : 9896,
						'entities' : 170,
						'media' : 35,
						'related_passages' : 6066,
						'articles' : 31730,
						'commentary' : 9963 
					},
					'subworks' : [
						{
							'title' : 'I',
							'guid' : '/aeneid/1/' 
						},
						{
							'title' : 'II',
							'guid' : '/aeneid/2/' 
						},
						{
							'title' : 'III',
							'guid' : '/aeneid/3/' 
						},
						{
							'title' : 'IV',
							'guid' : '/aeneid/4/' 
						},
						{
							'title' : 'V',
							'guid' : '/aeneid/5/' 
						},
						{
							'title' : 'VI',
							'guid' : '/aeneid/6/' 
						},
						{
							'title' : 'VII',
							'guid' : '/aeneid/7/' 
						},
						{
							'title' : 'VIII',
							'guid' : '/aeneid/8/' 
						},
						{
							'title' : 'IX',
							'guid' : '/aeneid/9/' 
						},
						{
							'title' : 'X',
							'guid' : '/aeneid/10/' 
						},
						{
							'title' : 'XI',
							'guid' : '/aeneid/10/' 
						},
						{
							'title' : 'XII',
							'guid' : '/aeneid/12/' 
						}
					]
				},
				{
					'title' : 'Georgics',
					'guid' : '/georgics/',
					'meta_elements' : {
						'lines' : 2188,
						'entities' : 83,
						'media' : 10,
						'related_passages' : 994,
						'articles' : 7219,
						'commentary' : 0 
					},
					'subworks' : [
						{
							'title' : 'I',
							'guid' : '/georgics/1/' 
						},
						{
							'title' : 'II',
							'guid' : '/georgics/2/' 
						},
						{
							'title' : 'III',
							'guid' : '/georgics/3/' 
						},
						{
							'title' : 'IV',
							'guid' : '/georgics/4/' 
						}
					]
				},
				{

					'title' : 'Eclogues',
					'guid' : '/eclogues/',
					'meta_elements' : {
						'lines' : 830,
						'entities' : 53,
						'media' : 9,
						'related_passages' : 349,
						'articles' : 5366,
						'commentary' : 0 
					},
					'subworks' : [
						{
							'title' : 'I',
							'guid' : '/eclogues/1/' 
						},
						{
							'title' : 'II',
							'guid' : '/eclogues/2/' 
						},
						{
							'title' : 'III',
							'guid' : '/eclogues/3/' 
						},
						{
							'title' : 'IV',
							'guid' : '/eclogues/4/' 
						},
						{
							'title' : 'V',
							'guid' : '/eclogues/5/' 
						},
						{
							'title' : 'VI',
							'guid' : '/eclogues/6/' 
						},
						{
							'title' : 'VII',
							'guid' : '/eclogues/7/' 
						},
						{
							'title' : 'VIII',
							'guid' : '/eclogues/8/' 
						},
						{
							'title' : 'IX',
							'guid' : '/eclogues/9/' 
						},
						{
							'title' : 'X',
							'guid' : '/eclogues/10/' 
						}
					]
				}
			]
		}];

		// Watch the line update and perform necessary post line update fns
		$scope.$watch(
			function () { return document.getElementById('demo-lines').innerHTML },
			function( value ){
			// Update to the DOM has occured;
				var val = value || null;

				if ( val && $(".line").length > 0 ){

					$scope.update_overflow();
					document.title = "Home | Segetes";

				}

			});

		// Do the inital query parsing the path values
		db.query( $scope, $http, params);



		// Show/hide the meta information for the lines
		$scope.toggle_meta = function(e){
			var $target = $(e.target)
			,	$line 	= $target.parents(".line")
			,	$lines  = $(".line")
			;

			// Toggle the meta information based on all the lines
			if($line.hasClass("with-meta")){

				$line.removeClass("with-meta");
				if(!($lines.hasClass("with-meta"))){
					$lines.removeClass("extended");
				}

			} else {

				$line.addClass("with-meta");

				if(!($lines.hasClass("extended"))){
					$lines.addClass("extended");
				}
			}

		};

		// Update the selected line depths each time height is changed 
		$scope.update_meta_line_depths = function(){
			Sg.with_meta_lines.forEach(function(e){
					e.h = $(".line[data-n='" + e.n + "']").offset().top;
				});
		};

		// Show/hide the selected meta section
		// Sections: definitions, comments, articles, related passages
		$scope.toggle_meta_section = function(e){
			var $target  = $(e.target)
			,	$line 	 = $target.parents(".line")
			,	$section = $line.children(".meta").children(".meta-section." + $target.data().type)
			;

			$target.toggleClass("selected");
			$section.toggleClass("meta-selected");
			$scope.update_meta_line_depths();


		};

		// Show/hide the scansion highlighting
		$scope.toggle_scansion = function(e){
			var $target = $(e.target)
			,	$line 	= $target.parents(".line")
			;
			$target.toggleClass("selected");
			$line.toggleClass("show-scansion");

		};

		$scope.update_overflow = function(){
			// Check commentary overflows
			$(".commentary .meta-item").each( function( k, elem ){
				if( !is_overflowed_h( elem ) && !$( elem ).hasClass("view-more")){
					$( elem ).children(".show-more-toggle").hide();
				}
			} );

			// Check definitions overflows
			$(".definitions .meta-item").each( function( k, elem ){
				if( !is_overflowed_h( elem ) && !$( elem ).hasClass("view-more") ) {
					$( elem ).children(".show-more-toggle").hide();
				}
			} );
		};

		$scope.show_more = function( e ){

			var $target = $(e.target)
			,	$parent = $target.parents(".meta-item")
			;

			if ( !$target.hasClass("show-more-toggle") ){
				$target = $target.parents(".show-more-toggle");
			}

			if ( $parent.hasClass( "view-more" ) ) {
				$parent.removeClass( "view-more" ); 
			}else{
				$parent.addClass( "view-more" );
			}

			$scope.update_meta_line_depths();

		};

		// Toggle the loading modal window
		$scope.toggle_loading = function( type ){
			var $loading = $("#loading_modal")
			;
			if ( type === "show" && $loading.hasClass("hidden")){
					$loading.removeClass( "hidden" );
			}else{
				if ( !$loading.hasClass( "hidden" ) ){
					$loading.addClass( "hidden" );
				}
			}
		};

		$scope.update_overflow = function(){
			// Check commentary overflows
			$(".commentary .meta-item").each( function( k, elem ){
				if( !is_overflowed_h( elem ) && !$( elem ).hasClass("view-more")){
					$( elem ).children(".show-more-toggle").hide();
				}
			} );

			// Check definitions overflows
			$(".definitions .meta-item").each( function( k, elem ){
				if( !is_overflowed_h( elem ) && !$( elem ).hasClass("view-more") ) {
					$( elem ).children(".show-more-toggle").hide();
				}
			} );
		};

		$scope.check_user_fetch = function(){

		};

    }
  ]);

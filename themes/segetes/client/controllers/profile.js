angular.module('sg')
	.controller('ProfileController', ['$scope', '$http', function($scope, $http) {

	$scope.my_notes = [];

	$scope.fetch_notes = function( ){
	// Get notes and bookmarked lines
		var fetch_notes_params = {};

		$http.post( '/fetch_notes', fetch_notes_params )
					.success(function(data, status, headers, config){
						//$(".bookmarked").removeClass("bookmarked").addClass("not-bookmarked");
						console.log("My Notes Data", data);

						$scope.my_notes = data;

					})
					.error(function(data, status, headers, config){
						console.log("Error:", data);
					});

	};

	$scope.fetch_notes();

}]);
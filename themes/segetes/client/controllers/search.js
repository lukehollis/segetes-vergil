angular.module('sg')
	.controller('SearchController', ['$scope', '$http', function($scope, $http) {


	var Sg = window.__sg__
	,	db = Sg.Db
	,	entity_params = {
					model : 'entities',
					__bind_method__ : "append",
					__resp__ : 'items',
					sort : {'en_name':1},
					offset : 0,
					limit : 21
				}
	,	media_params = {
					model : 'media',
					__bind_method__ : "append",
					__resp__ : 'items',
					sort : {'title':1},
					offset : 0,
					limit : 21
				}
	,	line_params = {
					model : 'lines',
					__bind_method__ : "append",
					sort : {'work':1,'subwork.n':1,'line.n':1},
					offset : 0,
					limit : 21
				}
	,	path = location.pathname.split("/")
	;

	$scope.lines = [];
	$scope.items = [];
	$scope.linked_apps = [];
	$scope.line_offset = 0;
	$scope.item_offset = 0;

	$scope.$watch(
		function () { return document.getElementById('result_lines').innerHTML },
		function( value ){
		// Update to the DOM has occured;
			var val = value || null;

			if ( val ){

				$("#loading_modal").addClass("hidden");
				document.title = "Search | Segetes";

			}

		});

	$scope.init = function(){
		$scope.query_linked();
	};

	$scope.update = function( e ){
		$scope.lines = [];
		$scope.items = [];

		 $("#loading_modal").removeClass("hidden");

		line_params['textsearch'] = $scope.query;
		//entity_params['en_name'] = $scope.query; 
		//media_params['title'] =  $scope.query; 

		// Query this application
		db.query($scope, $http, line_params);

		// Query all the linked applications
		$scope.linked_apps.forEach(function(app){


				$http.post('/api', line_params)
					.success(function(data, status, headers, config){

						console.log("Linked Texts Results:", data);
						app.lines = data.res; 

					})
					.error(function(data, status, headers, config){
						console.log("Error:", data);
					});


			});


		//db.query($scope, $http, entity_params);
		//db.query($scope, $http, media_params);

	};

	$scope.more = function( type, e ) {

		//$(".more").addClass("hidden");
		//$(".loading").removeClass("hidden");

		if ( type === "lines" ){
			$scope.line_offset = $scope.line_offset + 1;
			line_params.offset = $scope.line_offset * line_params.limit;
			db.query($scope, $http, line_params);

		}else if ( type === "items" ){

			//$scope.item_offset = $scope.item_offset + 1;
			//entity_params.offset = $scope.item_offset;
			//media_params.offset = $scope.item_offset;
			//db.query($scope, $http, entity_params);
			//db.query($scope, $http, media_params);

		}


	};


	$scope.query_linked = function(){
	// Query the linked apps

		var params = {
					model : 'linked_apps',
					limit : 0,
					__callback__ : $scope.fetch_linked
				};

		db.query( $scope, $http, params );

	};

	$scope.check_user_fetch = function(){
	// Check the user

	};

	$scope.init();

}]);
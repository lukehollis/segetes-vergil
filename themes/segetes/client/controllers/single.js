angular.module('sg')
	.controller('SingleController', ['$scope', '$http', function($scope, $http) {


	var Sg = window.__sg__
	,	db = Sg.Db
	,	geo = Sg.Geo
	,	path = location.pathname.split("/")
	,	params = {
					include_line_refs : true,
					model : path[1],
					slug : path[2]
				}
	,	lat = 0
	,	lon = 0
	;

	/*
	 * Managing primary update client application lifecycle
	 */
	$scope.update = function( e ){

		db.query($scope, $http, params);

	};

	/* 
	 * Apply response from single query
	 */
	$scope.bind = function() {
	//	debugger;

		$scope.items = $scope.entities || $scope.media; 
		if ( $scope.items.length > 0 ){
			$scope.item = $scope.items[0];

			if( typeof $scope.item.geo !== "undefined" ){
				$scope.init_geo();
			}

		}
	};

	/*
	 * If there's geo information for the single item content, load map
	 */
	$scope.init_geo = function(){

		if ( $("#map").length == 0 ){

			setTimeout( function(){geo.init($scope.item.geo.lat, $scope.item.geo.lon, $scope.item.geo.z)}, 500);
		}else{
			geo.init($scope.item.geo.lat, $scope.item.geo.lon, $scope.item.geo.z);
		}

	};

	/*
	 * Init for data-ng-init
	 */
	$scope.init = function(){

		params.__callback__ = $scope.bind;
		$scope.update();

	};




}]);

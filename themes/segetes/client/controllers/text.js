angular.module('sg')
	.controller('TextController', ['$scope', '$http', function($scope, $http) {

		var Sg 		= window.__sg__ 
		,	db 		= Sg.Db 
		;

		$scope.is_text_page = true;
		$scope.work_title = "";
		$scope.subwork_title = "";
		Sg.with_meta_lines = [];
		// Get user notes if user logged in
		Sg.my_notes = [];

		// Watch the line update and perform necessary post line update fns
		$scope.$watch(
			function () { return document.getElementById('lines').innerHTML },
			function( value ){
			// Update to the DOM has occured;
				var val = value || null;

				if ( val && $(".line").length > 0 ){

					$scope.toggle_loading( 'hide' );
					$scope.update_overflow();

				}

			});

		$scope.init = function(){

			// unhide translation link
			$(".translation-link").removeClass("hidden");

			// Do the first update for the page load
			$scope.update();
		};

		// Update the data based on the path params
		$scope.update = function(e) {

			var	path = location.pathname.split("/")
			,	params = {
					'model' : 'lines',
					'__callback__' : $scope.update_translations
				}
			,	path_lines
			,	i
			;

			path = path.clean("");

			if ( path.length >= 3 ){
				path_lines = path[2].split("-");

				if ( path_lines.length > 1 ){
					params['line.n'] = {
						$gte : parseInt( path_lines[0] ),
						$lte : parseInt( path_lines[1] )
					}
				}else {
					params['line.n'] = {
						$gte : parseInt( path_lines[0] )
					}
					params.limit = 14;
				}

				params['subwork.n'] = parseInt( path[1] );
				params['work'] = path[0]; 


			}else if ( path.length >= 2 ){
				params['work'] = path[0]; 
				params['subwork.n'] = parseInt( path[1] );
				params['line.n'] = {
					$gte : 1
				}
				params.limit = 14;

			}else if (path.length >= 1) {
				params['work'] = path[0]; 
				params['subwork.n'] = 1; 
				params['line.n'] = {
					$gte : 1
				}
				params.limit = 14;

			}else {
				params['work'] = ''; 
				params['subwork.n'] = 1; 
				params['line.n'] = {
					$gte : 1
				}
				params.limit = 14;

			}

			// Show the loading modal window 
			$scope.toggle_loading( 'show' );

			// Do the inital query parsing the path values
			db.query( $scope, $http, params);

			// Set the scope text location values; 
			$scope.set_selected_work( params.work );
			$scope.set_selected_subwork( params['subwork.n'] );
			if ( typeof params['line.n'] === "object" ){
				$scope.line_from = params['line.n']['$gte'];
			}else {
				$scope.line_from = params['line.n'];
			}
			$scope.set_pagination( params );

			$scope.check_user_fetch();

			$scope.scroll_to_top();
		};
		window.onpopstate = function( e ) {
			// Update scope with the line params
			$scope.update();		
		};

		// Show/hide the meta information for the lines
		$scope.toggle_meta = function(e){
			var $target = $(e.target)
			,	$line 	= $target.parents(".line")
			,	$lines  = $(".line")
			;

			// Toggle the meta information based on all the lines
			if($line.hasClass("with-meta")){

				$line.removeClass("with-meta");
				$scope.update_meta_line_depths();
				Sg.with_meta_lines = Sg.with_meta_lines
					.filter(function (line) {
						return line.n !== $line.data().n;
					});
				if ( $line.data().n === Sg.lemma_fixed_n ){
					delete Sg.lemma_fixed_n;
				}

				if(!($lines.hasClass("with-meta"))){
					$("header").removeClass("header-hidden");
					$lines.removeClass("extended");
				}

			} else {

				$line.addClass("with-meta");
				$scope.update_meta_line_depths();
				Sg.with_meta_lines.push({
						'n' : $line.data().n,
						'h' : $line.offset().top
					});
				Sg.with_meta_lines.sort(function(a, b){
						return a.h - b.h;
					});


				if(!($lines.hasClass("extended"))){
					$("header").addClass("header-hidden");
					$lines.addClass("extended");
				}
			}

		};

		// Update the selected line depths each time height is changed 
		$scope.update_meta_line_depths = function(){
			Sg.with_meta_lines.forEach(function(e){
					e.h = $(".line[data-n='" + e.n + "']").offset().top;
				});
		};

		// Show/hide the selected meta section
		// Sections: definitions, comments, articles, related passages
		$scope.toggle_meta_section = function(e){
			var $target  = $(e.target)
			,	$line 	 = $target.parents(".line")
			,	$section = $line.children(".meta").children(".meta-section." + $target.data().type)
			;

			$target.toggleClass("selected");
			$section.toggleClass("meta-selected");
			$scope.update_meta_line_depths();


		};

		// Show/hide the scansion highlighting
		$scope.toggle_scansion = function(e){
			var $target = $(e.target)
			,	$line 	= $target.parents(".line")
			;
			$target.toggleClass("selected");
			$line.toggleClass("show-scansion");

		};

		$scope.update_overflow = function(){
			// Check commentary overflows
			$(".commentary .meta-item").each( function( k, elem ){
				if( !is_overflowed_h( elem ) && !$( elem ).hasClass("view-more")){
					$( elem ).children(".show-more-toggle").hide();
				}
			} );

			// Check definitions overflows
			$(".definitions .meta-item").each( function( k, elem ){
				if( !is_overflowed_h( elem ) && !$( elem ).hasClass("view-more") ) {
					$( elem ).children(".show-more-toggle").hide();
				}
			} );

			// Check notes overflows
			$(".my_notes .meta-item").each( function( k, elem ){
				if( !is_overflowed_h( elem ) && !$( elem ).hasClass("view-more") ) {
					$( elem ).children(".show-more-toggle").hide();
				}
			} );
		};

		$scope.show_more = function( e ){

			var $target = $(e.target)
			,	$parent = $target.parents(".meta-item")
			;

			if ( !$target.hasClass("show-more-toggle") ){
				$target = $target.parents(".show-more-toggle");
			}

			if ( $parent.hasClass( "view-more" ) ) {
				$parent.removeClass( "view-more" ); 
			}else{
				$parent.addClass( "view-more" );
			}

			$scope.update_meta_line_depths();

		};

		// Toggle the loading modal window
		$scope.toggle_loading = function( type ){
			var $loading = $("#loading_modal")
			;
			if ( type === "show" && $loading.hasClass("hidden")){
					$loading.removeClass( "hidden" );
			}else{
				if ( !$loading.hasClass( "hidden" ) ){
					$loading.addClass( "hidden" );
				}
			}
		};

		// Toggle the browse tool panels
		$scope.toggle_browse_panel = function( e ) {
			var $target = $( e.target )
			,	$parent = $target.parents(".tool-browser")	
			,	$panel = $parent.children(".tool-panel")
			;

			if ( $panel.hasClass("hidden") ){
				$panel.removeClass("hidden");
			}else{
				$panel.addClass("hidden");
			}

		};

		// Browse the texts via AJAX
		$scope.browse = function( e ){
			var $target = $()
			,	new_path = '/'
			;	

			// Normalize event
			if ( e ){
				$target = $( e.target )
			}

			// Unfix all lines
			$(".lemma-fixed").removeClass("lemma-fixed");
			$(".header-hidden").removeClass("header-hidden");
			delete Sg.lemma_fixed_n;
			delete Sg.old_lemma_fixed_n;
			delete Sg.lemma_fixed_h;
			delete Sg.lemma_fixed_n_to_unfix;
			Sg.with_meta_lines = [];

			// Set the options based on the selected target 
			if ( $target.hasClass("work-option") ){
				$scope.set_selected_work( $target.data().slug );
				$scope.set_selected_subwork( 1 );
				$scope.line_from = 1;

			}else if ( $target.hasClass("subwork-option") ){
				$scope.set_selected_subwork( parseInt( $target.data().n ) );
				$scope.line_from = 1;

			}else {
				// else, it's a line update
				if ( $scope.line_from.length > 0 ) {
					$scope.line_from = $scope.check_line_input( $scope.line_from );
				}
			}


			// Finally, do a query with the new parameters
			db.query( $scope, $http, {
					'model' : "lines",
					'work' : $scope.work,
					'subwork.n' : $scope.subwork,
					'line.n': {
							$gte : parseInt( $scope.line_from ) 
						},
					'__callback__' : $scope.update_translations,
					'limit' : 14
				});
			$scope.set_pagination({
					'work' : $scope.work,
					'subwork.n' : $scope.subwork,
					'line.n' : $scope.line_from,
					'limit' : 14
				});

			new_path = new_path + $scope.work + "/" + $scope.subwork + "/" + $scope.line_from;
			history.pushState({
					'work' : $scope.work,
					'subwork.n' : $scope.subwork,
					'line.n': {
							$gte : parseInt( $scope.line_from ) 
						},
				}, "", new_path);

			// Finally, raise all panels
			$(".tool-panel").addClass("hidden");

			// Show the loading window to be unhidden on the line DOM manipulation
			$scope.toggle_loading( 'show' );

			$scope.scroll_to_top();
		};

		// Convert to Roman Numeral
		$scope.to_roman_numeral = function( n ){
			var l = "";
			if ( n === 1 ) {
				l = "I";
			}else if ( n === 2 ){
				l = "II";
			}else if ( n === 3 ){
				l = "III";
			}else if ( n === 4 ){
				l = "IV";
			}else if ( n === 5 ){
				l = "V";
			}else if ( n === 6 ){
				l = "VI";
			}else if ( n === 7 ){
				l = "VII";
			}else if ( n === 8 ){
				l = "VIII";
			}else if ( n === 9 ){
				l = "IX";
			}else if ( n === 10 ){
				l = "X";
			}else if ( n === 11 ){
				l = "XI";
			}else if ( n === 12 ){
				l = "XII";
			}
			return l;
		};

		// Set the selected work from the works
		$scope.set_selected_work = function( work ){
			if ( work == "aeneid" ) {
				$scope.selected_work = $scope.works[0];
			}else if ( work === "georgics" ){
				$scope.selected_work = $scope.works[1];
			}else if ( work === "eclogues" ){
				$scope.selected_work = $scope.works[2];
			}else {
				$scope.selected_work = null;
			} 

			$scope.work = work;
		};

		// Set the selected subwork via the roman numeral
		$scope.set_selected_subwork = function( subwork ){
			$scope.selected_subwork = {
											'n' : subwork,
											'title' : $scope.to_roman_numeral( subwork )
										};			
			$scope.subwork = subwork;
		};
		$scope.works = [
				{
					'title' : 'Aeneid',
					'guid' : '/aeneid/',
					'slug' : 'aeneid',	
					'meta_elements' : {
						'online' : 0,
						'entities' : 1023,
						'media' : 213,
						'related_passages' : 23875,
						'articles' : 5098 
					},
					'subworks' : [
						{
							'title' : 'I',
							'n' : 1, 
							'guid' : '/aeneid/1/' 
						},
						{
							'title' : 'II',
							'n' : 2, 
							'guid' : '/aeneid/2/' 
						},
						{
							'title' : 'III',
							'n' : 3, 
							'guid' : '/aeneid/3/' 
						},
						{
							'title' : 'IV',
							'n' : 4, 
							'guid' : '/aeneid/4/' 
						},
						{
							'title' : 'V',
							'n' : 5, 
							'guid' : '/aeneid/5/' 
						},
						{
							'title' : 'VI',
							'n' : 6, 
							'guid' : '/aeneid/6/' 
						},
						{
							'title' : 'VII',
							'n' : 7, 
							'guid' : '/aeneid/7/' 
						},
						{
							'title' : 'VIII',
							'n' : 8, 
							'guid' : '/aeneid/8/' 
						},
						{
							'title' : 'IX',
							'n' : 9, 
							'guid' : '/aeneid/9/' 
						},
						{
							'title' : 'X',
							'n' : 10, 
							'guid' : '/aeneid/10/' 
						},
						{
							'title' : 'XI',
							'n' : 11, 
							'guid' : '/aeneid/10/' 
						},
						{
							'title' : 'XII',
							'n' : 12, 
							'guid' : '/aeneid/12/' 
						}
					]
				},
				{
					'title' : 'Georgics',
					'guid' : '/georgics/',
					'slug' : 'georgics',
					'meta_elements' : {
						'online' : 0,
						'entities' : 83,
						'media' : 31,
						'related_passages' : 9018,
						'articles' : 341 
					},
					'subworks' : [
						{
							'title' : 'I',
							'n' : 1, 
							'guid' : '/georgics/1/' 
						},
						{
							'title' : 'II',
							'n' : 2, 
							'guid' : '/georgics/2/' 
						},
						{
							'title' : 'III',
							'n' : 3, 
							'guid' : '/georgics/3/' 
						},
						{
							'title' : 'IV',
							'n' : 4, 
							'guid' : '/georgics/4/' 
						}
					]
				},
				{

					'title' : 'Eclogues',
					'guid' : '/eclogues/',
					'slug' : 'eclogues',
					'meta_elements' : {
						'online' : 0,
						'entities' : 163,
						'media' : 28,
						'related_passages' : 658,
						'articles' : 234 
					},
					'subworks' : [
						{
							'title' : 'I',
							'n' : 1, 
							'guid' : '/eclogues/1/' 
						},
						{
							'title' : 'II',
							'n' : 2, 
							'guid' : '/eclogues/2/' 
						},
						{
							'title' : 'III',
							'n' : 3, 
							'guid' : '/eclogues/3/' 
						},
						{
							'title' : 'IV',
							'n' : 4, 
							'guid' : '/eclogues/4/' 
						},
						{
							'title' : 'V',
							'n' : 5, 
							'guid' : '/eclogues/5/' 
						},
						{
							'title' : 'VI',
							'n' : 6, 
							'guid' : '/eclogues/6/' 
						},
						{
							'title' : 'VII',
							'n' : 7, 
							'guid' : '/eclogues/7/' 
						},
						{
							'title' : 'VIII',
							'n' : 8, 
							'guid' : '/eclogues/8/' 
						},
						{
							'title' : 'IX',
							'n' : 9, 
							'guid' : '/eclogues/9/' 
						},
						{
							'title' : 'X',
							'n' : 10, 
							'guid' : '/eclogues/10/' 
						}
					]
				}
			];
		$scope.set_selected_work( $scope.work );
		$scope.set_selected_subwork( $scope.subwork );

		// Check line input for only ints, only four digits
		$scope.check_line_input = function( val ) {

			if ( !( /^\d+$/.test(val) ) ){ 
				val = val.replace(/\D/g,'');
			}

			if ( val.length > 4 ){
				val = val.substring(0,4);
			}

			return val;
		};

		$scope.set_pagination = function( params ){
			var lengths = {
					'aeneid' : [756,804,718,705,871,901,817,731,818,908,915,952],
					'georgics' : [514,542,566,566],
					'eclogues' : [84,73,111,63,90,86,70,109,67,77]
				}
			,	work_length = lengths[ params.work ][ params['subwork.n'] - 1 ] 
			;


			// Prev pagination
			$scope.pag = $scope.pag || { prev : { to : 0, from : 0, link : "/" }, next : { to : 0, from : 0, link : "/" } };

			// Ensure line_from is integer
			$scope.line_from = parseInt( $scope.line_from );
			if ( $scope.line_from < work_length ){

				if ( $scope.line_from > 0 && $scope.line_from < params.limit && $scope.line_from !== 1 ){
					$scope.pag.prev.to = params.limit; 
				}else if ( $scope.line_from > 0 ){
					$scope.pag.prev.to = $scope.line_from;
				}else {
					$scope.pag.prev.to = 1;
				}
				if ( $scope.line_from - (params.limit + 1) > 0 ){
					$scope.pag.prev.from = $scope.line_from - (params.limit + 1);
				}else {
					$scope.pag.prev.from = 1;
				}
				$scope.pag.prev.link =  "/" + params.work + "/" + params['subwork.n'] + "/" + $scope.pag.prev.from;

				// Next Pagination
				if ( $scope.line_from + params.limit < work_length ){
					$scope.pag.next.from = $scope.line_from + params.limit;
				}else {
					$scope.pag.next.from = work_length;
				}
				if ( $scope.line_from + (params.limit + params.limit) < work_length ){
					$scope.pag.next.to = $scope.line_from + (params.limit + params.limit);
				}else {
					$scope.pag.next.to = work_length;
				}
				$scope.pag.next.link =  "/" + params.work + "/" + params['subwork.n'] + "/" + $scope.pag.next.from;
			}else {
				$scope.pag.prev.from = 0;
				$scope.pag.prev.to = 0;
				$scope.pag.next.from = 0;
				$scope.pag.next.to = 0;
			}


		};

		$scope.upload_media = function( e ){

			if( $scope.check_upload_form( e ) ){

				$http.post('/send_modal', $scope.upload)
					.success(function(data, status, headers, config){
						$scope.upload_modal_toggle( {} );
						$scope.success_modal_toggle();
					});
			}

		};

		$scope.check_upload_form = function( e ){


			return true;
		};

		$scope.success_modal_toggle = function(){
			$("#success_modal").toggleClass("hidden");
		};

		$scope.upload_modal_toggle = function( e ){
			var $target 
			,	$line
			;

			if ( typeof e.target !== "undefined" ) {
				$target = $(e.target);

				if ( !$target.hasClass("modal-close") ){
					$line = $target.parents( ".line" );
					$scope.upload = {
						'line_n' : $line.data().n,
						'work' : $.trim( $( "h1.work_title" ).text().toLowerCase() ),
						'subwork' : $.trim( $( "h2.subwork_title" ).text() )
					};
				}
			}

			$("#upload_modal").toggleClass("hidden");
		};

		$scope.modal_hide = function( e ){
		// Hide whatever modal
			$(".text-modal").addClass("hidden");

		};

		$scope.fetch_notes = function( ){
		// Get notes and bookmarked lines
			var fetch_notes_params = {
					'ref.work' : $scope.work,
					'ref.subwork.n' : $scope.subwork,
					'ref.line.n' : {
								'$gte' : $scope.line_from,
								'$lte' : $scope.line_from + 14 
							}
				};

			$http.post( '/fetch_notes', fetch_notes_params )
						.success(function(data, status, headers, config){
							//$(".bookmarked").removeClass("bookmarked").addClass("not-bookmarked");
							console.log("My Notes Data", data);

							$scope.lines.forEach(function(line){

								line.my_notes = [];
								data.forEach(function(note){
									if ( parseInt( note.ref.line.n ) === line.line.n){
										$(".line[data-n='" + note.ref.line.n + "'] .not-bookmarked").addClass("bookmarked").removeClass("not-bookmarked");
										line.my_notes.push( note );
									}
								});

							});

							$scope.update_overflow();
						})
						.error(function(data, status, headers, config){
							console.log("Error:", data);
						});

		};

		$scope.add_note = function( e ){
		// Process Note params, toggle bookmarked, and post to /save_note
			var $target = $( e.target )
			;

			if ( !$target.hasClass("meta-tab") ){
				$target = $target.parents(".meta-tab");
			}
			$scope.note = {
							ref : $scope.get_note_ref( $target ),
							text:""
						};

			if ( $target.hasClass("not-bookmarked") ){
				$("#save_note_modal").removeClass("hidden");
			}

			if ( $target.hasClass("bookmarked") ){
			// If it already has a note on the line, get the note on the line
				$scope.get_update_note( e );

			}

		};

		$scope.get_update_note = function( e ){

			var $target = $( e.target )
			,	fetch_notes_params = {
					'ref.work' : $scope.work,
					'ref.subwork.n' : $scope.subwork,
					'ref.line.n' : $target.parents(".line").data().n
				}
			;

			$http.post( '/fetch_notes', fetch_notes_params )
						.success(function(data, status, headers, config){
							//$(".bookmarked").removeClass("bookmarked").addClass("not-bookmarked");
							console.log("My Notes Update Data", data);

							data.forEach(function(note){
								$scope.note = note;
							});
							$("#update_note_modal").removeClass("hidden");

						})
						.error(function(data, status, headers, config){
							console.log("Error:", data);
						});
		};

		$scope.get_note_ref = function( $target ){
		// Get the note params from the target selected on the line
			var ref = {
					work : '',
					subwork : {
							n : 0
						},
					line : {
							n : 0
						}
				}
			;

			ref.work = $scope.work;
			ref.subwork.n = $scope.subwork;
			ref.line.n = $target.parents(".line").data().n

			return ref;
		};

		$scope.save_note = function( ){
		// save the note
			$http.post('/save_note', $scope.note)
						.success(function(data, status, headers, config){
							console.log("Note saved!");
							$("#save_note_modal").addClass("hidden");
							$scope.fetch_notes();

						})
						.error(function(data, status, headers, config){
							alert("We're sorry, it looks there was a problem saving your note. Please let us know what happened at contact@segetes.io, and we'll take a look at it.")
							console.log("Error:", data);
						});

		};

		$scope.update_note = function( ){
		// update the note on the line
			$http.post('/update_note', $scope.note)
						.success(function(data, status, headers, config){
							console.log("Note updated!");
							$("#update_note_modal").addClass("hidden");
							$scope.fetch_notes();
						})
						.error(function(data, status, headers, config){
							alert("We're sorry, it looks there was a problem saving your note. Please let us know what happened at contact@segetes.io, and we'll take a look at it.")
							console.log("Error:", data);
						});

		};

		$scope.remove_note = function( e ){
		// post to remove note to remove a note
			var $target = $( e.target )
			;

			$scope.note = {
							ref : {subwork:{},line:{}},
							text:""
						};

			$scope.note.ref.work = $scope.work;
			$scope.note.ref.subwork.n = $scope.subwork;
			$scope.note.ref.line.n = $target.parents(".line").data().n;



			$http.post('/remove_note', $scope.note)
						.success(function(data, status, headers, config){
							console.log("Note removed:", data);
							$(".line[data-n='" + $scope.note.ref.line.n + "'] .bookmarked").addClass("not-bookmarked").removeClass("bookmarked");
							$scope.fetch_notes();
						})
						.error(function(data, status, headers, config){
							alert("We're sorry, it looks there was a problem removing your note. Please let us know what happened at contact@segetes.io, and we'll take a look at it.")
							console.log("Error:", data);
						});

		};

		$scope.check_user_fetch = function() {
		// Check if a user is logged in
			$http.post('/auth/check', $scope.note)
				.success(function(data, status, headers, config){
					if (data === true){
						$scope.fetch_notes();
					}
				})
				.error(function(data, status, headers, config){
					console.log("Error:", data);
				});

		};

		$scope.toggle_translation = function() {
			// toggle translation 

			$("#loading_modal").addClass("hidden");
			$("#translation_panel").fadeIn(200);
			$(".translation-tool").addClass(".translation-active");

		};

		$scope.get_translations = function(){
			var translation_params = {
					model : "translations",
					work : $scope.work,
					'subwork.n' : $scope.subwork,
					'edition.slug' : $scope.selected_translation_edition,
					'line.n' : {
								'$gte' : $scope.selected_trans_range.min,
								'$lte' : $scope.selected_trans_range.max 
							}//,
					//'__callback__' : $scope.update_translations
				};

			//$("#loading_modal").removeClass("hidden");
			db.query($scope, $http, translation_params);

		};

		$scope.update_translations = function(){
		// Gather some metadata from the lines about the avaiable translations
			var available_editions = []
			;

			// First get all the available translation editions and thier max/min
			$scope.lines.forEach(function(line){

					line.translations.forEach(function(trans){

						var is_in_av_ed = false;

						available_editions.forEach(function(ed){
								if ( ed.slug === trans.edition.slug ) {
									is_in_av_ed = true;
									ed.line_ns.push.apply( ed.line_ns, trans.l_ns );
								}

							});

						if ( !is_in_av_ed ) {
							available_editions.push({
									slug : trans.edition.slug, 
									line_ns : trans.l_ns
								});
						}


					});

				});

			// Then set the selected translation
			$scope.available_editions = available_editions;
			$scope.set_selected_translation();

		};

		$scope.set_selected_translation = function( e ){
		// Set the selected translation for the translation pane
			var min = 0
			,	max = 0
			, 	selected_translation = {}
			,	sel_ed = ''
			,	$target
			;

			if ( e && typeof e.target !== "undefined" ) {
				// Update the selected translation based on the event
				$target = $(e.target);

				if ( !$target.hasClass("option") ){
					$target = $target.parents(".option");
				}

				sel_ed = $target.data().edition;
				$scope.available_editions.forEach(function(ed){
						if ( ed.slug === sel_ed ){
							selected_translation = ed;
						}
					});

			}else {
				// Otherwise, set the default selected translation
				if ( $scope.available_editions.length > 0 ){
					selected_translation = $scope.available_editions[0];
				}

			}

			$scope.selected_translation_edition = selected_translation.slug; 

			min = Math.min.apply(null, selected_translation.line_ns),
			max = Math.max.apply(null, selected_translation.line_ns);


			$scope.selected_trans_range = {
					min : min,
					max : max
				};

			$scope.get_translations();

		};

		$scope.close_translation = function(){
			$("#translation_panel").fadeOut(200);
			$(".translation-active").removeClass(".translation-active");

		};

		$scope.scroll_to_top = function() {
		// scroll to the top of the page
			$('html, body').animate({scrollTop : 0},200);
		};


		$scope.init();

    }
  ]);

Array.prototype.clean = function(deleteValue) {
  for (var i = 0; i < this.length; i++) {
    if (this[i] == deleteValue) {         
      this.splice(i, 1);
      i--;
    }
  }
  return this;
};

function is_overflowed_h(element){
    return element.scrollHeight > element.clientHeight + 10;
}

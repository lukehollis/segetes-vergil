angular.module('sg')
	.controller('TextIndexController', ['$scope', '$http', function($scope, $http) {

		var Sg = window.__sg__
		,	db = Sg.Db
		,	author_params = {type:"authors"}
		;

		$scope.authors = [{
			'display_name' : 'Vergil',
			'guid' : '/author/vergil',
			'texts' : [
				{
					'title' : 'Aeneid',
					'guid' : '/aeneid/',
					'meta_elements' : {
						'lines' : 9896,
						'entities' : 170,
						'media' : 0,
						'related_passages' : 6066,
						'articles' : 31730,
						'commentary' : 0 
					},
					'subworks' : [
						{
							'title' : 'I',
							'guid' : '/aeneid/1/' 
						},
						{
							'title' : 'II',
							'guid' : '/aeneid/2/' 
						},
						{
							'title' : 'III',
							'guid' : '/aeneid/3/' 
						},
						{
							'title' : 'IV',
							'guid' : '/aeneid/4/' 
						},
						{
							'title' : 'V',
							'guid' : '/aeneid/5/' 
						},
						{
							'title' : 'VI',
							'guid' : '/aeneid/6/' 
						},
						{
							'title' : 'VII',
							'guid' : '/aeneid/7/' 
						},
						{
							'title' : 'VIII',
							'guid' : '/aeneid/8/' 
						},
						{
							'title' : 'IX',
							'guid' : '/aeneid/9/' 
						},
						{
							'title' : 'X',
							'guid' : '/aeneid/10/' 
						},
						{
							'title' : 'XI',
							'guid' : '/aeneid/10/' 
						},
						{
							'title' : 'XII',
							'guid' : '/aeneid/12/' 
						}
					]
				},
				{
					'title' : 'Georgics',
					'guid' : '/georgics/',
					'meta_elements' : {
						'lines' : 2188,
						'entities' : 83,
						'media' : 0,
						'related_passages' : 994,
						'articles' : 7219,
						'commentary' : 0 
					},
					'subworks' : [
						{
							'title' : 'I',
							'guid' : '/georgics/1/' 
						},
						{
							'title' : 'II',
							'guid' : '/georgics/2/' 
						},
						{
							'title' : 'III',
							'guid' : '/georgics/3/' 
						},
						{
							'title' : 'IV',
							'guid' : '/georgics/4/' 
						}
					]
				},
				{

					'title' : 'Eclogues',
					'guid' : '/eclogues/',
					'meta_elements' : {
						'lines' : 830,
						'entities' : 53,
						'media' : 0,
						'related_passages' : 349,
						'articles' : 5366,
						'commentary' : 0 
					},
					'subworks' : [
						{
							'title' : 'I',
							'guid' : '/eclogues/1/' 
						},
						{
							'title' : 'II',
							'guid' : '/eclogues/2/' 
						},
						{
							'title' : 'III',
							'guid' : '/eclogues/3/' 
						},
						{
							'title' : 'IV',
							'guid' : '/eclogues/4/' 
						},
						{
							'title' : 'V',
							'guid' : '/eclogues/5/' 
						},
						{
							'title' : 'VI',
							'guid' : '/eclogues/6/' 
						},
						{
							'title' : 'VII',
							'guid' : '/eclogues/7/' 
						},
						{
							'title' : 'VIII',
							'guid' : '/eclogues/8/' 
						},
						{
							'title' : 'IX',
							'guid' : '/eclogues/9/' 
						},
						{
							'title' : 'X',
							'guid' : '/eclogues/10/' 
						}
					]
				}
			]
		}];

		$scope.init = function(){
			$scope.query_linked();
		};


		$scope.query_linked = function(){

			var params = {
						model : 'linked_apps',
						limit : 0,
						__callback__ : $scope.fetch_linked
					};

			db.query( $scope, $http, params );

		};

		$scope.fetch_linked = function(){


			$scope.linked_apps.forEach(function(app){

					app.authors = [];

					$http.post( app.url + "/api", { manifest : true } )
						.success(function(data, status, headers, config){
							console.log("Linked Text Response:", data);

							app.authors = data.res;

						})
						.error(function(data, status, headers, config){
							console.log('Error', data);
						});

				});



		};

		$scope.init();
    }
  ]);

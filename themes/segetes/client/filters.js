angular.module('sgFilters', [])
	.filter('capitalize', function() {
		return function(input, all) {
			return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
		}
	})
	.filter('slugify', function() {
		return function(input, all) {
			return input 
			    .toLowerCase()
			    .replace(/[^\w ]+/g,'')
			    .replace(/ +/g,'-')
			    ;
		}
	})
	.filter('thumb_small', function() {
		return function(input, all) {
			return (!!input) ? input.replace( ".", "-small.") : '';
		}
	})
	.filter('thumb_large', function() {
		return function(input, all) {
			return (!!input) ? input.replace( ".", "-large.") : '';
		}
	})
	.filter('thumb_large_uncropped', function() {
		return function(input, all) {
			return (!!input) ? input.replace( ".", "-large-uncropped.") : '';
		}
	})
	.filter('roman_numeral', function() {
		return function( n, all){
			var l = "";
			if ( n === 1 ) {
				l = "I";
			}else if ( n === 2 ){
				l = "II";
			}else if ( n === 3 ){
				l = "III";
			}else if ( n === 4 ){
				l = "IV";
			}else if ( n === 5 ){
				l = "V";
			}else if ( n === 6 ){
				l = "VI";
			}else if ( n === 7 ){
				l = "VII";
			}else if ( n === 8 ){
				l = "VIII";
			}else if ( n === 9 ){
				l = "IX";
			}else if ( n === 10 ){
				l = "X";
			}else if ( n === 11 ){
				l = "XI";
			}else if ( n === 12 ){
				l = "XII";
			}
			return l;
		}

	})
	.filter('from_roman_numeral', function() {
		return function( title, all){
			var l = ""
			,	n = 1	
			;

			title = title.split(" ");
			if( title.length > 0 ){
				n = title[0];
			}

			if ( n === "I" ) {
				l = 1;
			}else if ( n === "II" ){
				l = 2;
			}else if ( n === "III" ){
				l = 3;
			}else if ( n === "IV" ){
				l = 4;
			}else if ( n === "V" ){
				l = 5;
			}else if ( n === "VI" ){
				l = 6;
			}else if ( n === "VII" ){
				l = 7;
			}else if ( n === "VIII" ){
				l = 8;
			}else if ( n === "IX" ){
				l = 9;
			}else if ( n === "X" ){
				l = 10;
			}else if ( n === "XI" ){
				l = 11;
			}else if ( n === "XII" ){
				l = 12;
			}
			return l;
		}

	})
	.filter('unsafe', function($sce) { 
		return $sce.trustAsHtml; 
	});

 

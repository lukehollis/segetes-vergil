/*
 *
 * db.js - custom functions for querying the application api 
 *
 */

(function($){

window.__sg__ = window.__sg__ || {};
var Sg = window.__sg__;

Sg.Db = {

	/* 
     * Save post data to database (currently not used)
     */
		save : function($scope, $http, params){

						console.log("Save:", params);

						$http.post('/api/s', params)
							.success(function(data, status, headers, config){
								//console.log(data);
							})
							.error(function(data, status, headers, config){
								//console.log('error', data);
							});

					}
	/* 
     * Query data from database and set custom callbacks, scope assignment, etc. 
     */
	,	query : function($scope, $http, params){

						var callback
						;

						console.log("Query:", params);

						// set callback from the params
						if ( typeof params.__callback__ !== "undefined" ){
							callback = params.__callback__;
							delete params.__callback__;
						}

						// If the Segetes object is set to is_mobile, enforce query limit of 5
						if ( typeof Sg.is_mobile !== "undefined" ){
							if ( Sg.is_mobile === true && params.model === "lines" ){
								params.limit = 5;
							}

						// Otherwise, if the browser window is small enough to be mobile 
						} else if ( window.innerWidth < 600 && params.model === "lines" ){
								params.limit = 5;
						}

						// Do post to API with params
						$http.post('/api', params)
							.success(function(data, status, headers, config){

								// Set the key on scope that the response should be set
								if ( typeof data.__resp__ !== "undefined" ){
									data.model = data.__resp__;
								}

								// Set the no more items flag
								if ( typeof data.limit !== "undefined" ){
									if ( data.res.length <= data.limit - 1 ){
										$scope.no_more_items = true;
									}
								}else {
									if ( data.res.length <= 0 ){
										$scope.no_more_items = true;
									}
								}
								// Hide modal window just in case there's no items
								if ( data.res.length === 0 ){
									$("#loading_modal").addClass("hidden");
								}

								// If it is with lines, parse the line text as JSON
								if ( typeof data.include_line_refs !== "undefined" ){
									data.res.forEach(function(e){
										if ( typeof e.lines !== "undefined" ){
											e.lines = JSON.parse( e.lines );

										}
									});
								}

								console.log("Response:", data);

								// Check the phase to apply the data based on the posted bind_method
								if(!$scope.$$phase){
									$scope.$apply(function(){

										if ( data.__bind_method__ === "append" ){
											data.res.forEach(function(e){
												$scope[ data.model ].push( e );
											});

										}else {

											$scope[data.model] = data.res;
										}

									});
								}else {

									if ( data.__bind_method__ === "append" ){
										data.res.forEach(function(e){
											$scope[ data.model ].push( e );
										});

									}else {
										$scope[data.model] = data.res;
									}

								}

								// If it's a lines query, update the titles to reflect the lines
								if ( data.model == "lines" && data.res.length > 0 ){

									$scope.work_title = data.res[0].work.charAt(0).toUpperCase() + data.res[0].work.slice(1);
									$scope.subwork_title = data.res[0].subwork.title.charAt(0).toUpperCase() + data.res[0].subwork.title.slice(1);
									document.title = $scope.work_title + " " + $scope.subwork_title + " | Segetes";
									
									// If it's a lines query and the user is logged in, also fetch the nodes
									$scope.check_user_fetch();

								}

								// If there's a callback, then do the callback
								if ( typeof callback !== "undefined" ){
									callback();
								}

							})

							// Otherwise, handle the errors from the server 
							.error(function(error, status, headers, config){

								console.log("Error:", error);

							});

			}
	};

})( jQuery );

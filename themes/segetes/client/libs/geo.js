/*
 *
 * geo.js - for handling mapping issues 
 *
 */

(function($){

window.__sg__ = window.__sg__ || {};
var Sg = window.__sg__;

Sg.Geo = {
		init : function(lat, lon, z, set_marker){
			z = typeof z !== "undefined" ? z : 7;
			set_marker = typeof set_marker !== "undefined" ? set_marker : true;

			var center = new google.maps.LatLng( lat, lon );

			// Create an array of styles.
			var styles = [
				{
					stylers: [
						{ hue: "#d9d6bd" },
						{ saturation: 0 }
					]
				},{
					featureType: "road",
					elementType: "geometry",
					stylers: [
						{ hue: "#482f00" },
						{ visibility: "simplified" }
					]
				},{
					featureType: "highway",
					elementType: "geometry",
					stylers: [
						{ hue: "#482f00" },
						{ saturation: -50 },
						{ visibility: "simplified" }
					]
				},{
					featureType: "road",
					elementType: "labels",
					stylers: [
						{ visibility: "off" }
					]
				},{
				    featureType: "poi.business",
				    elementType: "labels",
				    stylers: [
				      { visibility: "off" }
				    ]
				},{
				    featureType: "water",
				    elementType: "geometry",
				    stylers: [
						{ lightness: 100 },
						{ visibility: "simplified" }
				    ]
				},{
				    featureType: "transit",
				    elementType: "labels",
				    stylers: [
				      { visibility: "off" }
				    ]
				},{
				    featureType: "transit",
				    elementType: "geo",
				    stylers: [
				      { visibility: "off" }
				    ]
				  }
			];

			// Create a new StyledMapType object, passing it the array of styles,
			// as well as the name to be displayed on the map type control.
			var styledMap = new google.maps.StyledMapType(styles,
										{name: "Styled Map"});

			// Create a map object, and include the MapTypeId to add
			// to the map type control.
			var mapOptions = {
					zoom: z,
					center: center,
					disableDefaultUI: true,
					mapTypeControlOptions: {
						mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
					}
				};
			var map = new google.maps.Map(document.getElementById('map'),
				mapOptions);

			//Associate the styled map with the MapTypeId and set it to display.
			map.mapTypes.set('map_style', styledMap);
			map.setMapTypeId('map_style');
			// Set the map tilt
			map.setTilt(45);


			if ( set_marker ){
				var latLong = new google.maps.LatLng(lat, lon);
				var marker = new google.maps.Marker({
				  position: latLong,
				  map: map//,
				  //title: name,
				  //post_id : e.post_id,
				  //icon: icon_image
				});
			}



			return map;

		}
	};

})( jQuery );

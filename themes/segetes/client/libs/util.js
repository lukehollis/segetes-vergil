/*
 *
 * util.js - segetes custom util helps 
 *
 */

(function($){

window.__sg__ = window.__sg__ || {};
var Sg = window.__sg__;

Sg.Util = {

	toggle_nav_fix : function($target, fix_type) {

		if (fix_type === "fix") {
			if (!($target.hasClass("fixed"))){
				$target.addClass("fixed");
			}
		}else{
			if ($target.hasClass("fixed")){
				$target.removeClass("fixed");
			}
		}
	},

	slugify : function( string ) {
		return string 
		    .toLowerCase()
		    .replace(/[^\w ]+/g,'')
		    .replace(/ +/g,'-')
		    ;
	}


}

})( jQuery );

/*
 *
 * Site.js - site specific functions for the aweb portfolio
 *
 */

(function($){
angular.element(document).ready(function() {

window.__sg__ = window.__sg__ || {};
var Sg = window.__sg__;

// fix iOS font related issues 
if(navigator.userAgent.match(/iPad/i)){
	$("html").addClass("ipad");
}

// js - modernizer
$('.no-js').removeClass('no-js').addClass('js');

Sg.close_filler_modal = function() {
  $("#filler_modal").addClass("hidden");
}
//if (document.cookie.indexOf("segetesBetaModal") < 0) {
	//document.cookie = "segetesBetaModal=true; expires=Fri, 2 Feb 2015 23:59:59 GMT; path=/";
	//$("#filler_modal").removeClass("hidden");
//}

$(".close-link").on("click", function(){
	Sg.close_filler_modal();	
});

/*
 * Scroll interactions for index
 */

// establish scroll variables
Sg.curr_depth = $(window).scrollTop();
Sg.prev_depth = 0;
Sg.winHeight = window.innerHeight ? window.innerHeight : $(window).height();
Sg.target_depth = 0;
Sg.header_hidden = false;
Sg.with_meta_lines = [];

// Scroll event
$(document).on("scroll touchstart", function(e) {

	Sg.prev_depth = Sg.curr_depth;
	Sg.curr_depth = $(window).scrollTop();

	// Down scroll event
	if(Sg.prev_depth < Sg.curr_depth){

		if(Sg.curr_depth > Sg.target_depth){
			//Sg.Lib.toggle_nav_fix($target, 'fix');

			// Toggle the header hidden information
			if (Sg.header_hidden == false){
				Sg.header_hidden = true;
			}

		}

		// Regulate the fixed lines on the single page
		do_fixed_lines();

	// Up scroll event
	}else {

		if(Sg.curr_depth < Sg.target_depth){
			//Sg.Lib.toggle_nav_fix($target, 'unfix');

			if (Sg.header_hidden == true){
				Sg.header_hidden = false;
			}

		}

		// Regulate the fixed lines on the single page
		do_fixed_lines();

	}
});

function do_fixed_lines(){


	// Check for sticky lines
	// Set the old lemma to compare against the new
	if (typeof Sg.lemma_fixed_n !== "undefined" ){
		Sg.old_lemma_fixed_n = Sg.lemma_fixed_n;
	}

	// Get the element with the highest offset that is greater than the current offset
	Sg.with_meta_lines.forEach(function(e){
		if( e.h < Sg.curr_depth ){
			Sg.lemma_fixed_n = e.n;
			Sg.lemma_fixed_h = e.h;
		}else if ( e.n === Sg.lemma_fixed_n ){
			// If the current offset is greater than the lemma, mark it to be unfixed 
			if (Sg.lemma_fixed_h > Sg.curr_depth){
				Sg.lemma_fixed_n_to_unfix = e.n;
			}
		}
	});

	// If there's an old lemma that doesn't equal the current lemma, remove it. 
	if ( typeof Sg.old_lemma_fixed_n !== "undefined" ){
		if ( Sg.old_lemma_fixed_n !== Sg.lemma_fixed_n ){
			$(".line[data-n='" + Sg.old_lemma_fixed_n + "'] .lemma").removeClass("lemma-fixed");
		}
	}

	// If we have a lemma to fix
	if ( typeof Sg.lemma_fixed_n !== "undefined" ){
		// If we have a lemma to unfix and it's the currently fixed lemma
		if ( typeof Sg.lemma_fixed_n_to_unfix !== "undefined" && Sg.lemma_fixed_n === Sg.lemma_fixed_n_to_unfix ) {

			$(".line[data-n='" + Sg.lemma_fixed_n + "'] .lemma").removeClass("lemma-fixed");
			Sg.lemma_fixed_n_to_unfix = 0;

		// Otherwise, fix the lemma
		}else {
			$(".line[data-n='" + Sg.lemma_fixed_n + "'] .lemma").addClass("lemma-fixed");
		}
	}
}

/*
 *  Side Menu 
 */
Sg.side_menu_shown = false;
$("body").on("click", function(e){
	var $target = $(e.target)
	,	$panel = $(".side-menu-panel")
	;
	console.log($target);

	// Toggle the side menu if click on side-menu-head 
	if ( 
			$target.parents(".side-menu").length > 0 
		|| $target.parents(".side-menu-head").length > 0 
		|| $target.hasClass("side-menu") === true 
		){ 

		$('.side-menu-panel').toggleClass('shown');
		if ( Sg.side_menu_shown ){
			Sg.side_menu_shown = false;
		}else{
			Sg.side_menu_shown = true;
		}

	// Else if it's a click on the side menu interior, leave open
	} else if ( 
			$target.parents(".side-menu-panel").length > 0
		|| $target.hasClass("side-menu-panel") === true 
		){
		// Do nothing

	// Close if click not on .side-menu-head
	} else {
		if ( Sg.side_menu_shown ){
			Sg.side_menu_shown = false; 
			$('.side-menu-panel.shown').removeClass('shown');
		}
	}

	// If this is a click on the body, close the text-browser panels
	if (
			$target.parents(".tool-browser").length === 0 
		){
			$(".tool-panel").addClass("hidden");
	}

	if ( $target.hasClass("login") || $target.parents(".login").length > 0 ){
		$(".modal-login").addClass("modal-lowered");
		e.preventDefault();
	}


});


// Login controls 
$(".modal-login").on("click", function(e){
	var $target = $(e.target)
	;
	if($target.parents(".modal-inner").length == 0){
		e.preventDefault();
		$(".modal-login").removeClass("modal-lowered");
	}
});

// Detect Mobile
Sg.is_mobile = window.innerWidth < 600;

});
})(jQuery);
